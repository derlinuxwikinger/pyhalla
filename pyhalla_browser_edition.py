#!/usr/bin/python3

import pygame
from random import randrange
#import pygame_menu
#from pygame_menu import sound
from pygame import mixer
import random
import os
from random import randint
from typing import Tuple, Any
import asyncio

### Function for collecting data
def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

### Init pygame
pygame.init()

### Set font type depending on OS
pixel_font = pygame.font.SysFont('', 60)

### Init music
pygame.mixer.init(44100, -16, 2, 4096)

### Variables
clock = pygame.time.Clock()
global FPS
FPS = 100
global difficulty
difficulty = 1
global character
character = 2

### Raise number of sound channels to 20
pygame.mixer.set_num_channels(20)

### Set Cooldown
global OS_COOLDOWN
OS_COOLDOWN = 2500

### Window resolution
W = 640
H = 360
screen = pygame.display.set_mode((W,H), vsync=1)
### Name of the window
pygame.display.set_caption('Pyhalla')

### Load background image
bg1 = pygame.image.load(resource_path('images/bg1.png')).convert_alpha()
#bg1 = pygame.transform.scale(bg1,(W,H))
bg2 = pygame.image.load(resource_path('images/bg2.png')).convert_alpha()
#bg2 = pygame.transform.scale(bg2,(W,H))
### Load sky background
sky1 = pygame.image.load(resource_path('images/sky1.jpg')).convert_alpha()
#sky1 = pygame.transform.scale(sky1,(W,H))
sky2 = pygame.image.load(resource_path('images/sky2.jpg')).convert_alpha()
#sky2 = pygame.transform.scale(sky2,(W,H))

def rotate_center(image, angle):
    orig_rect = image.get_rect()
    rotate_image = pygame.transform.rotate(image, angle)
    rotate_rect = orig_rect.copy()
    rotate_rect.center = rotate_image.get_rect().center
    rotate_image = rotate_image.subsurface(rotate_rect).copy()
    return rotate_image

### Load player character
class Player(pygame.sprite.Sprite):
    def __init__(self):

        ### Starting Position of players character
        self.char_x = 0
        self.char_y = 450
        self.index = 0
        self.index_sail = 0
        self.index_viking_player = 0
        self.ANIMATION_COOLDOWN = 0
        self.ANIMATION_COOLDOWN_SAIL = 0
        self.ANIMATION_COOLDOWN_VIKING_PLAYER = 0
        self.ANIMATION_LENGTH = 1000
        self.player_is_alive = True

        ### Player Life
        self.player_is_invincible = False
        self.invincible_init = 0
        self.invincible_timer = 150
        self.player_lifes = 3

        ### Additional variables
        self.smooth_movement_up = 0
        self.smooth_movement_down = 0
        self.smooth_movement_left = 0
        self.smooth_movement_right = 0

        ### Load images
        if character == 1:
            self.player_char_viking_horn = pygame.image.load(resource_path('images/player_char/player_char_viking_horn.png')).convert_alpha()
            self.player_char_viking_lookup = pygame.image.load(resource_path('images/player_char/player_char_viking_lookup.png')).convert_alpha()
            self.player_char_viking_lookdown = pygame.image.load(resource_path('images/player_char/player_char_viking_lookdown.png')).convert_alpha()
            self.player_ship_shadow = pygame.image.load(resource_path('images/ship_shadow.png')).convert_alpha()
            self.player_img_default = pygame.image.load(resource_path('images/player_char/player_char11.png')).convert_alpha()
            self.player_img1 = pygame.image.load(resource_path('images/player_char/player_char1.png')).convert_alpha()
            self.player_img2 = pygame.image.load(resource_path('images/player_char/player_char2.png')).convert_alpha()
            self.player_img3 = pygame.image.load(resource_path('images/player_char/player_char3.png')).convert_alpha()
            self.player_img4 = pygame.image.load(resource_path('images/player_char/player_char4.png')).convert_alpha()
            self.player_img5 = pygame.image.load(resource_path('images/player_char/player_char5.png')).convert_alpha()
            self.player_img6 = pygame.image.load(resource_path('images/player_char/player_char6.png')).convert_alpha()
            self.player_img7 = pygame.image.load(resource_path('images/player_char/player_char7.png')).convert_alpha()
            self.player_img8 = pygame.image.load(resource_path('images/player_char/player_char8.png')).convert_alpha()
            self.player_img9 = pygame.image.load(resource_path('images/player_char/player_char9.png')).convert_alpha()
            self.player_img10 = pygame.image.load(resource_path('images/player_char/player_char10.png')).convert_alpha()
            self.player_img11 = pygame.image.load(resource_path('images/player_char/player_char11.png')).convert_alpha()
            self.animation_images = []
            self.animation_images.append(self.player_img11)
            self.animation_images.append(self.player_img10)
            self.animation_images.append(self.player_img9)
            self.animation_images.append(self.player_img8)
            self.animation_images.append(self.player_img7)
            self.animation_images.append(self.player_img6)
            self.animation_images.append(self.player_img5)
            self.animation_images.append(self.player_img4)
            self.animation_images.append(self.player_img3)
            self.animation_images.append(self.player_img2)
            self.animation_images.append(self.player_img1)
            ### Get hitbox of image
            self.rect_player = self.player_img_default.get_rect()
            self.rect_player.x = 20
            self.rect_player.y = 220
            ### Make hitbox smaller
            self.rect_player.width = 170
            self.rect_player.height = 40
            ### Ship sails
            self.ship_sail_img1 = pygame.image.load(resource_path('images/ship_sail/sail1.png')).convert_alpha()
            self.ship_sail_img2 = pygame.image.load(resource_path('images/ship_sail/sail2.png')).convert_alpha()
            self.ship_sail_images = []
            self.ship_sail_images.append(self.ship_sail_img1)
            self.ship_sail_images.append(self.ship_sail_img2)
            ### Viking player
            self.player_char_viking_images_img1 = pygame.image.load(resource_path('images/player_char/player_char_viking1.png')).convert_alpha()
            self.player_char_viking_images_img2 = pygame.image.load(resource_path('images/player_char/player_char_viking2.png')).convert_alpha()
            self.player_char_viking_images_img3 = pygame.image.load(resource_path('images/player_char/player_char_viking3.png')).convert_alpha()
            self.player_char_viking_images_img4 = pygame.image.load(resource_path('images/player_char/player_char_viking4.png')).convert_alpha()
            self.player_char_viking_images = []
            self.player_char_viking_images.append(self.player_char_viking_images_img1)
            self.player_char_viking_images.append(self.player_char_viking_images_img2)
            self.player_char_viking_images.append(self.player_char_viking_images_img3)
            self.player_char_viking_images.append(self.player_char_viking_images_img4)

        elif character == 2:
            self.player_ship_shadow = pygame.image.load(resource_path('images/ship_shadow.png')).convert_alpha()
            self.player_img_default = pygame.image.load(resource_path('images/player_char.png')).convert_alpha()
            self.dancing_img1 = pygame.image.load(resource_path('images/dancing/dancing1.png')).convert_alpha()
            self.dancing_img2 = pygame.image.load(resource_path('images/dancing/dancing2.png')).convert_alpha()
            self.dancing_img3 = pygame.image.load(resource_path('images/dancing/dancing3.png')).convert_alpha()
            self.dancing_img4 = pygame.image.load(resource_path('images/dancing/dancing4.png')).convert_alpha()
            self.dancing_img5 = pygame.image.load(resource_path('images/dancing/dancing5.png')).convert_alpha()
            self.dancing_img6 = pygame.image.load(resource_path('images/dancing/dancing6.png')).convert_alpha()
            self.hopping_img1 = pygame.image.load(resource_path('images/hopping/hopping1.png')).convert_alpha()
            self.hopping_img2 = pygame.image.load(resource_path('images/hopping/hopping2.png')).convert_alpha()
            self.hopping_img3 = pygame.image.load(resource_path('images/hopping/hopping3.png')).convert_alpha()
            self.hopping_img4 = pygame.image.load(resource_path('images/hopping/hopping4.png')).convert_alpha()
            self.hopping_img5 = pygame.image.load(resource_path('images/hopping/hopping5.png')).convert_alpha()
            self.hopping_img6 = pygame.image.load(resource_path('images/hopping/hopping6.png')).convert_alpha()
            self.laola_img1 = pygame.image.load(resource_path('images/laola/laola1.png')).convert_alpha()
            self.laola_img2 = pygame.image.load(resource_path('images/laola/laola2.png')).convert_alpha()
            self.laola_img3 = pygame.image.load(resource_path('images/laola/laola3.png')).convert_alpha()
            self.laola_img4 = pygame.image.load(resource_path('images/laola/laola4.png')).convert_alpha()
            self.laola_img5 = pygame.image.load(resource_path('images/laola/laola5.png')).convert_alpha()
            self.laola_img6 = pygame.image.load(resource_path('images/laola/laola6.png')).convert_alpha()
            self.laola_img7 = pygame.image.load(resource_path('images/laola/laola7.png')).convert_alpha()
            self.laola_img8 = pygame.image.load(resource_path('images/laola/laola8.png')).convert_alpha()
            self.laola_img9 = pygame.image.load(resource_path('images/laola/laola9.png')).convert_alpha()
            self.animation_images = []
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.dancing_img1)
            self.animation_images.append(self.dancing_img2)
            self.animation_images.append(self.dancing_img3)
            self.animation_images.append(self.dancing_img4)
            self.animation_images.append(self.dancing_img5)
            self.animation_images.append(self.dancing_img6)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.hopping_img1)
            self.animation_images.append(self.hopping_img2)
            self.animation_images.append(self.hopping_img3)
            self.animation_images.append(self.hopping_img4)
            self.animation_images.append(self.hopping_img5)
            self.animation_images.append(self.hopping_img6)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.player_img_default)
            self.animation_images.append(self.laola_img1)
            self.animation_images.append(self.laola_img2)
            self.animation_images.append(self.laola_img3)
            self.animation_images.append(self.laola_img4)
            self.animation_images.append(self.laola_img5)
            self.animation_images.append(self.laola_img6)
            self.animation_images.append(self.laola_img7)
            self.animation_images.append(self.laola_img8)
            self.animation_images.append(self.laola_img9)
            ### Ship sails
            self.ship_sail_img1 = pygame.image.load(resource_path('images/ship_sail/sail1.png')).convert_alpha()
            self.ship_sail_img2 = pygame.image.load(resource_path('images/ship_sail/sail2.png')).convert_alpha()
            self.ship_sail_images = []
            self.ship_sail_images.append(self.ship_sail_img1)
            self.ship_sail_images.append(self.ship_sail_img2)
            ### Get hitbox of image
            self.rect_player = self.player_img_default.get_rect()
            self.rect_player.y = 220
            self.rect_player.x = 15
            ### Make hitbox smaller
            self.rect_player.width = 170
            self.rect_player.height = 40

    def spawn_character(self):
        if character == 1:
            ### Ship shadow
            screen.blit(self.player_ship_shadow, (self.char_x - 30, self.char_y - 275))
            ### Sail animation
            screen.blit(self.ship_sail_img1, (self.char_x - 30, self.char_y - 350))
            if self.ANIMATION_COOLDOWN_SAIL > 100:
                self.index_sail += 1
                self.ANIMATION_COOLDOWN_SAIL = 0
            if self.index_sail >= len(self.ship_sail_images):
                self.index_sail = 0
            self.ship_sail_img1 = self.ship_sail_images[self.index_sail]
            self.ANIMATION_COOLDOWN_SAIL += 15
            ### player viking
            screen.blit(self.player_char_viking_images_img1, (self.char_x + 120, self.char_y - 252.5))
            if self.ANIMATION_COOLDOWN_VIKING_PLAYER > 100:
                self.index_viking_player += 1
                self.ANIMATION_COOLDOWN_VIKING_PLAYER = 0
            if self.index_viking_player >= len(self.player_char_viking_images):
                self.index_viking_player = 0
            self.player_char_viking_images_img1 = self.player_char_viking_images[self.index_viking_player]
            self.ANIMATION_COOLDOWN_VIKING_PLAYER += 10
            ### Ship animation
            screen.blit(self.player_img_default, (self.char_x, self.char_y - 250))
            if self.ANIMATION_COOLDOWN > 100:
                self.index += 1
                self.ANIMATION_COOLDOWN = 0
            if self.index >= len(self.animation_images):
                self.index = 0
            self.player_img_default = self.animation_images[self.index]
            self.ANIMATION_COOLDOWN += 20

        elif character == 2:
            ### Ship shadow
            screen.blit(self.player_ship_shadow, (self.char_x - 40, self.char_y - 270))
            ### Sail animation
            screen.blit(self.ship_sail_img1, (self.char_x - 40, self.char_y - 350))
            if self.ANIMATION_COOLDOWN_SAIL > 100:
                self.index_sail += 1
                self.ANIMATION_COOLDOWN_SAIL = 0
            if self.index_sail >= len(self.ship_sail_images):
                self.index_sail = 0
            self.ship_sail_img1 = self.ship_sail_images[self.index_sail]
            self.ANIMATION_COOLDOWN_SAIL += 15
            ### Ship animation
            screen.blit(self.player_img_default, (self.char_x, self.char_y - 250))
            if self.ANIMATION_COOLDOWN > 100:
                self.index += 1
                self.ANIMATION_COOLDOWN = 0
            if self.index >= len(self.animation_images):
                self.index = 0
            self.player_img_default = self.animation_images[self.index]
            self.ANIMATION_COOLDOWN += 20

        #pygame.draw.rect(screen, 'red', (self.rect_player.x, self.rect_player.y, self.rect_player.width, self.rect_player.height), 2)

    def moving_up(self):
        if character == 2:
            if special_ability == True and self.char_y > 350:
                if 0 <= self.smooth_movement_up < 20:
                    player.char_y -= 2
                    self.rect_player.y -= 2
                    self.smooth_movement_up += 2
                if 20 <= self.smooth_movement_up < 40:
                    player.char_y -= 4
                    self.rect_player.y -= 4
                    self.smooth_movement_up += 2
                if 40 <= self.smooth_movement_up < 60:
                    player.char_y -= 6
                    self.rect_player.y -= 6
                    self.smooth_movement_up += 2
                if 60 <= self.smooth_movement_up < 80:
                    player.char_y -= 8
                    self.rect_player.y -= 8
                    self.smooth_movement_up += 2
                if 80 <= self.smooth_movement_up < 100:
                    player.char_y -= 10
                    self.rect_player.y -= 10
                    self.smooth_movement_up += 2
                if self.smooth_movement_up == 100:
                    player.char_y -= 12
                    self.rect_player.y -= 12
            elif special_ability == False and self.char_y > 350:
                if 0 <= self.smooth_movement_up < 20:
                    player.char_y -= 1
                    self.rect_player.y -= 1
                    self.smooth_movement_up += 2
                if 20 <= self.smooth_movement_up < 40:
                    player.char_y -= 2
                    self.rect_player.y -= 2
                    self.smooth_movement_up += 2
                if 40 <= self.smooth_movement_up < 60:
                    player.char_y -= 3
                    self.rect_player.y -= 3
                    self.smooth_movement_up += 2
                if 60 <= self.smooth_movement_up < 80:
                    player.char_y -= 4
                    self.rect_player.y -= 4
                    self.smooth_movement_up += 2
                if 80 <= self.smooth_movement_up < 100:
                    player.char_y -= 5
                    self.rect_player.y -= 5
                    self.smooth_movement_up += 2
                if self.smooth_movement_up == 100:
                    player.char_y -= 6
                    self.rect_player.y -= 6

        elif character == 1:
            if self.char_y > 350:
                if 0 <= self.smooth_movement_up < 20:
                    player.char_y -= 1
                    self.rect_player.y -= 1
                    self.ANIMATION_COOLDOWN += 40
                    self.smooth_movement_up += 2
                    self.player_char_viking_images_img1 = self.player_char_viking_lookup
                if 20 <= self.smooth_movement_up < 40:
                    player.char_y -= 2
                    self.rect_player.y -= 2
                    self.ANIMATION_COOLDOWN += 40
                    self.smooth_movement_up += 2
                    self.player_char_viking_images_img1 = self.player_char_viking_lookup
                if 40 <= self.smooth_movement_up < 60:
                    player.char_y -= 3
                    self.rect_player.y -= 3
                    self.ANIMATION_COOLDOWN += 40
                    self.smooth_movement_up += 2
                    self.player_char_viking_images_img1 = self.player_char_viking_lookup
                if 60 <= self.smooth_movement_up < 80:
                    player.char_y -= 4
                    self.rect_player.y -= 4
                    self.ANIMATION_COOLDOWN += 40
                    self.smooth_movement_up += 2
                    self.player_char_viking_images_img1 = self.player_char_viking_lookup
                if 80 <= self.smooth_movement_up < 100:
                    player.char_y -= 5
                    self.rect_player.y -= 5
                    self.ANIMATION_COOLDOWN += 40
                    self.smooth_movement_up += 2
                    self.player_char_viking_images_img1 = self.player_char_viking_lookup
                if self.smooth_movement_up == 100:
                    player.char_y -= 6
                    self.rect_player.y -= 6
                    self.ANIMATION_COOLDOWN += 40
                    self.player_char_viking_images_img1 = self.player_char_viking_lookup

    def moving_down(self):
        if character == 2:
            if special_ability == True and player.char_y < 550:
                if 0 <= self.smooth_movement_down < 20:
                    player.char_y += 2
                    self.rect_player.y += 2
                    self.smooth_movement_down += 2
                if 20 <= self.smooth_movement_down < 40:
                    player.char_y += 4
                    self.rect_player.y += 4
                    self.smooth_movement_down += 2
                if 40 <= self.smooth_movement_down < 60:
                    player.char_y += 6
                    self.rect_player.y += 6
                    self.smooth_movement_down += 2
                if 60 <= self.smooth_movement_down < 80:
                    player.char_y += 8
                    self.rect_player.y += 8
                    self.smooth_movement_down += 2
                if 80 <= self.smooth_movement_down < 100:
                    player.char_y += 10
                    self.rect_player.y += 10
                    self.smooth_movement_down += 2
                if self.smooth_movement_down == 100:
                    player.char_y += 12
                    self.rect_player.y += 12
            elif special_ability == False and player.char_y < 550:
                if 0 <= self.smooth_movement_down < 20:
                    player.char_y += 1
                    self.rect_player.y += 1
                    self.smooth_movement_down += 2
                if 20 <= self.smooth_movement_down < 40:
                    player.char_y += 2
                    self.rect_player.y += 2
                    self.smooth_movement_down += 2
                if 40 <= self.smooth_movement_down < 60:
                    player.char_y += 3
                    self.rect_player.y += 3
                    self.smooth_movement_down += 2
                if 60 <= self.smooth_movement_down < 80:
                    player.char_y += 4
                    self.rect_player.y += 4
                    self.smooth_movement_down += 2
                if 80 <= self.smooth_movement_down < 100:
                    player.char_y += 5
                    self.rect_player.y += 5
                    self.smooth_movement_down += 2
                if self.smooth_movement_down == 100:
                    player.char_y += 6
                    self.rect_player.y += 6

        elif character == 1:
            if player.char_y < 550:
                if player.char_x <= W - 240:
                    if 0 <= self.smooth_movement_down < 20:
                        player.char_y += 1
                        self.rect_player.y += 1
                        self.ANIMATION_COOLDOWN += 40
                        self.smooth_movement_down += 2
                        self.player_char_viking_images_img1 = self.player_char_viking_lookdown
                    if 20 <= self.smooth_movement_down < 40:
                        player.char_y += 2
                        self.rect_player.y += 2
                        self.ANIMATION_COOLDOWN += 40
                        self.smooth_movement_down += 2
                        self.player_char_viking_images_img1 = self.player_char_viking_lookdown
                    if 40 <= self.smooth_movement_down < 60:
                        player.char_y += 3
                        self.rect_player.y += 3
                        self.ANIMATION_COOLDOWN += 40
                        self.smooth_movement_down += 2
                        self.player_char_viking_images_img1 = self.player_char_viking_lookdown
                    if 60 <= self.smooth_movement_down < 80:
                        player.char_y += 4
                        self.rect_player.y += 4
                        self.ANIMATION_COOLDOWN += 40
                        self.smooth_movement_down += 2
                        self.player_char_viking_images_img1 = self.player_char_viking_lookdown
                    if 80 <= self.smooth_movement_down < 100:
                        player.char_y += 5
                        self.rect_player.y += 5
                        self.ANIMATION_COOLDOWN += 40
                        self.smooth_movement_down += 2
                        self.player_char_viking_images_img1 = self.player_char_viking_lookdown
                    if self.smooth_movement_down == 100:
                        player.char_y += 6
                        self.rect_player.y += 6
                        self.ANIMATION_COOLDOWN += 40
                        self.player_char_viking_images_img1 = self.player_char_viking_lookdown

    def moving_right(self):
        if character == 2:
            if special_ability == True and player.char_x <= W - 220:
                if 0 <= self.smooth_movement_right < 20:
                    player.char_x += 2
                    self.rect_player.x += 2
                    self.smooth_movement_right += 2
                if 20 <= self.smooth_movement_right < 40:
                    player.char_x += 4
                    self.rect_player.x += 4
                    self.smooth_movement_right += 2
                if 40 <= self.smooth_movement_right < 60:
                    player.char_x += 6
                    self.rect_player.x += 6
                    self.smooth_movement_right += 2
                if 60 <= self.smooth_movement_right < 80:
                    player.char_x += 8
                    self.rect_player.x += 8
                    self.smooth_movement_right += 2
                if 80 <= self.smooth_movement_right < 100:
                    player.char_x += 10
                    self.rect_player.x += 10
                    self.smooth_movement_right += 2
                if self.smooth_movement_right == 100:
                    player.char_x += 12
                    self.rect_player.x += 12
            elif special_ability == False and player.char_x <= W - 220:
                if 0 <= self.smooth_movement_right < 20:
                    player.char_x += 1
                    self.rect_player.x += 1
                    self.smooth_movement_right += 2
                if 20 <= self.smooth_movement_right < 40:
                    player.char_x += 2
                    self.rect_player.x += 2
                    self.smooth_movement_right += 2
                if 40 <= self.smooth_movement_right < 60:
                    player.char_x += 3
                    self.rect_player.x += 3
                    self.smooth_movement_right += 2
                if 60 <= self.smooth_movement_right < 80:
                    player.char_x += 4
                    self.rect_player.x += 4
                    self.smooth_movement_right += 2
                if 80 <= self.smooth_movement_right < 100:
                    player.char_x += 5
                    self.rect_player.x += 5
                    self.smooth_movement_right += 2
                if self.smooth_movement_right == 100:
                    player.char_x += 6
                    self.rect_player.x += 6

        elif character == 1:
            if player.char_x <= W - 220:
                if 0 <= self.smooth_movement_right < 20:
                    player.char_x += 1
                    self.rect_player.x += 1
                    self.ANIMATION_COOLDOWN += 40
                    self.smooth_movement_right += 2
                if 20 <= self.smooth_movement_right < 40:
                    player.char_x += 2
                    self.rect_player.x += 2
                    self.ANIMATION_COOLDOWN += 40
                    self.smooth_movement_right += 2
                if 40 <= self.smooth_movement_right < 60:
                    player.char_x += 3
                    self.rect_player.x += 3
                    self.ANIMATION_COOLDOWN += 40
                    self.smooth_movement_right += 2
                if 60 <= self.smooth_movement_right < 80:
                    player.char_x += 4
                    self.rect_player.x += 4
                    self.ANIMATION_COOLDOWN += 40
                    self.smooth_movement_right += 2
                if 80 <= self.smooth_movement_right < 100:
                    player.char_x += 5
                    self.rect_player.x += 5
                    self.ANIMATION_COOLDOWN += 40
                    self.smooth_movement_right += 2
                if self.smooth_movement_right == 100:
                    player.char_x += 6
                    self.rect_player.x += 6
                    self.ANIMATION_COOLDOWN += 40

    def moving_left(self):
        if character == 2:
            if special_ability == True and player.char_x > 0:
                if 0 <= self.smooth_movement_left < 20:
                    player.char_x -= 2
                    self.rect_player.x -= 2
                    self.smooth_movement_left += 2
                if 20 <= self.smooth_movement_left < 40:
                    player.char_x -= 4
                    self.rect_player.x -= 4
                    self.smooth_movement_left += 2
                if 40 <= self.smooth_movement_left < 60:
                    player.char_x -= 6
                    self.rect_player.x -= 6
                    self.smooth_movement_left += 2
                if 60 <= self.smooth_movement_left < 80:
                    player.char_x -= 8
                    self.rect_player.x -= 8
                    self.smooth_movement_left += 2
                if 80 <= self.smooth_movement_left < 100:
                    player.char_x -= 10
                    self.rect_player.x -= 10
                    self.smooth_movement_left += 2
                if self.smooth_movement_left == 100:
                    player.char_x -= 12
                    self.rect_player.x -= 12
            elif special_ability == False and player.char_x > 0:
                if 0 <= self.smooth_movement_left < 20:
                    player.char_x -= 1
                    self.rect_player.x -= 1
                    self.smooth_movement_left += 2
                if 20 <= self.smooth_movement_left < 40:
                    player.char_x -= 2
                    self.rect_player.x -= 2
                    self.smooth_movement_left += 2
                if 40 <= self.smooth_movement_left < 60:
                    player.char_x -= 3
                    self.rect_player.x -= 3
                    self.smooth_movement_left += 2
                if 60 <= self.smooth_movement_left < 80:
                    player.char_x -= 4
                    self.rect_player.x -= 4
                    self.smooth_movement_left += 2
                if 80 <= self.smooth_movement_left < 100:
                    player.char_x -= 5
                    self.rect_player.x -= 5
                    self.smooth_movement_left += 2
                if self.smooth_movement_left == 100:
                    player.char_x -= 6
                    self.rect_player.x -= 6

        elif character == 1:
            if player.char_x > 0:
                if 0 <= self.smooth_movement_left < 20:
                    player.char_x -= 1
                    self.rect_player.x -= 1
                    self.smooth_movement_left += 2
                    self.player_img_default = self.player_img7
                if 20 <= self.smooth_movement_left < 40:
                    player.char_x -= 2
                    self.rect_player.x -= 2
                    self.smooth_movement_left += 2
                    self.player_img_default = self.player_img7
                if 40 <= self.smooth_movement_left < 60:
                    player.char_x -= 3
                    self.rect_player.x -= 3
                    self.smooth_movement_left += 2
                    self.player_img_default = self.player_img7
                if 60 <= self.smooth_movement_left < 80:
                    player.char_x -= 4
                    self.rect_player.x -= 4
                    self.smooth_movement_left += 2
                    self.player_img_default = self.player_img7
                if 80 <= self.smooth_movement_left < 100:
                    player.char_x -= 5
                    self.rect_player.x -= 5
                    self.smooth_movement_left += 2
                    self.player_img_default = self.player_img7
                if self.smooth_movement_left == 100:
                    player.char_x -= 6
                    self.rect_player.x -= 6
                    self.player_img_default = self.player_img7

    def special_ability(self):
        if character == 1:
            self.player_ship_shadow.set_alpha(128)
            self.player_img_default.set_alpha(128)
            self.ship_sail_img1.set_alpha(128)
            self.player_char_viking_images_img1.set_alpha(128)
            self.player_char_viking_lookup.set_alpha(128)
            self.player_char_viking_lookdown.set_alpha(128)
            self.player_char_viking_images_img1 = self.player_char_viking_horn
        elif character == 2:
            pass

### Load obstacle images
class Obstacle():
    def __init__(self, speed, image):

        self.obstacle_speed = speed
        self.image = image
        self.index = 0
        self.ANIMATION_COOLDOWN = 0
        self.init_wait = 0

        #self.obstacle_img = pygame.image.load('images/fireball1.png').convert_alpha()
        self.color = (255, 0, 0)

        '''
        ### Background object viking
        self.viking_x_pos = W
        self.viking_y_pos = randrange(240, 250)
        self.viking1_first_spawn = random.randint(50, 300)
        self.viking2_first_spawn = random.randint(50, 600)
        self.viking3_first_spawn = random.randint(50, 900)
        self.viking4_first_spawn = random.randint(50, 1200)
        self.viking5_first_spawn = random.randint(50, 1500)
        self.first_spawn_wait_viking1 = 0
        self.first_spawn_wait_viking2 = 0
        self.first_spawn_wait_viking3 = 0
        self.first_spawn_wait_viking4 = 0
        self.first_spawn_wait_viking5 = 0
        self.first_spawn_vikings = True

        ### Background object raven
        self.raven2_background_x_pos = -40
        self.raven2_background_y_pos = randrange(10, int(180))
        self.play_sound_raven2 = True
        self.raven2_first_spawn = random.randint(200, 700)
        self.background_x_pos = W
        self.background_y_pos = randrange(10, int(180))
        self.wait = random.randint(50, 300)
        self.init_wait = 0
        self.first_spawn = random.randint(100, 300)
        self.first_spawn_wait = 0
        self.play_sound_raven = True

        ### Background object sleipnir
        self.sleipnir_background_x_pos_left = -40
        self.sleipnir_background_x_pos_right = W
        self.left_to_right = True
        self.sleipnir_background_y_pos = randrange(10, int(180))
        self.play_sound_sleipnir = True
        self.sleipnir_first_spawn = random.randint(500, 800)
        self.wait_sleipnir = random.randint(1500, 2000)
        self.init_wait = 0
        self.first_spawn_wait_sleipnir = 0

        if self.image == 'fireball':
            ### Load images
            self.obstacle_img1 = pygame.image.load(resource_path('images/fireball1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/fireball2.png')).convert_alpha()
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            ### Additional variables
            self.obstacle_x_pos = W
            self.obstacle_y_pos = randrange(H // 3, int(H * 0.95))
            self.obstacle_img = pygame.image.load(resource_path('images/fireball1.png')).convert_alpha()
            self.rect_fireball = self.obstacle_img.get_rect()
            self.rect_fireball.x = W
            self.rect_fireball.y = self.obstacle_y_pos
            self.play_sound_fire = True
        '''

        if self.image == 'rock':
            ### Load image
            self.obstacle_img = pygame.image.load(resource_path('images/rock.png')).convert_alpha()
            ### Additional variables
            self.obstacle_x_pos = W
            self.obstacle_y_pos = randrange(H // 3, int(H * 0.95))
            self.rect_rock = self.obstacle_img.get_rect()
            self.rect_rock.x = W
            self.rect_rock.y = self.obstacle_y_pos

        elif self.image == 'axe':
            ### Load images
            self.obstacle_img1 = pygame.image.load(resource_path('images/axe1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/axe2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/axe3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/axe4.png')).convert_alpha()
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            ### Additional variables
            self.obstacle_x_pos = W
            self.obstacle_y_pos = randrange(H // 3, int(H * 0.95))
            self.obstacle_img = pygame.image.load(resource_path('images/axe1.png')).convert_alpha()
            self.rect_axe = self.obstacle_img.get_rect()
            self.rect_axe.x = W
            self.rect_axe.y = self.obstacle_y_pos
            self.play_sound_axe = True

        elif self.image == 'sea_monster':
            ### Load images
            self.obstacle_img1 = pygame.image.load(resource_path('images/sea_monster/sea_monster1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/sea_monster/sea_monster2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/sea_monster/sea_monster3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/sea_monster/sea_monster4.png')).convert_alpha()
            self.obstacle_img5 = pygame.image.load(resource_path('images/sea_monster/sea_monster5.png')).convert_alpha()
            self.obstacle_img6 = pygame.image.load(resource_path('images/sea_monster/sea_monster6.png')).convert_alpha()
            self.obstacle_img7 = pygame.image.load(resource_path('images/sea_monster/sea_monster7.png')).convert_alpha()
            self.obstacle_img8 = pygame.image.load(resource_path('images/sea_monster/sea_monster8.png')).convert_alpha()
            self.obstacle_img9 = pygame.image.load(resource_path('images/sea_monster/sea_monster9.png')).convert_alpha()
            self.obstacle_img10 = pygame.image.load(resource_path('images/sea_monster/sea_monster10.png')).convert_alpha()
            self.obstacle_img11 = pygame.image.load(resource_path('images/sea_monster/sea_monster11.png')).convert_alpha()
            self.obstacle_img12 = pygame.image.load(resource_path('images/sea_monster/sea_monster12.png')).convert_alpha()
            self.obstacle_img13 = pygame.image.load(resource_path('images/sea_monster/sea_monster13.png')).convert_alpha()
            self.obstacle_img14 = pygame.image.load(resource_path('images/sea_monster/sea_monster14.png')).convert_alpha()
            self.obstacle_img15 = pygame.image.load(resource_path('images/sea_monster/sea_monster15.png')).convert_alpha()
            self.obstacle_img16 = pygame.image.load(resource_path('images/sea_monster/sea_monster16.png')).convert_alpha()
            self.obstacle_img17 = pygame.image.load(resource_path('images/sea_monster/sea_monster17.png')).convert_alpha()
            self.obstacle_img18 = pygame.image.load(resource_path('images/sea_monster/sea_monster18.png')).convert_alpha()
            self.obstacle_img19 = pygame.image.load(resource_path('images/sea_monster/sea_monster19.png')).convert_alpha()
            self.obstacle_img20 = pygame.image.load(resource_path('images/sea_monster/sea_monster20.png')).convert_alpha()
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            self.obstacle_images.append(self.obstacle_img5)
            self.obstacle_images.append(self.obstacle_img6)
            self.obstacle_images.append(self.obstacle_img7)
            self.obstacle_images.append(self.obstacle_img8)
            self.obstacle_images.append(self.obstacle_img9)
            self.obstacle_images.append(self.obstacle_img10)
            self.obstacle_images.append(self.obstacle_img11)
            self.obstacle_images.append(self.obstacle_img12)
            self.obstacle_images.append(self.obstacle_img13)
            self.obstacle_images.append(self.obstacle_img14)
            self.obstacle_images.append(self.obstacle_img15)
            self.obstacle_images.append(self.obstacle_img16)
            self.obstacle_images.append(self.obstacle_img17)
            self.obstacle_images.append(self.obstacle_img18)
            self.obstacle_images.append(self.obstacle_img19)
            self.obstacle_images.append(self.obstacle_img20)
            ### Additional variables
            self.sea_monster_x_pos = randrange(10, W - 200)
            self.sea_monster_y_pos = randrange(H // 3, int(H * 0.50))
            self.obstacle_img = pygame.image.load(resource_path('images/sea_monster/sea_monster1.png')).convert_alpha()
            self.rect_sea_monster = self.obstacle_img.get_rect()
            self.rect_sea_monster.x = W
            self.rect_sea_monster.y = self.sea_monster_y_pos
            self.wait_sea_monster = 400  ### Cooldown for the sea monster to appear
            self.wait_init = 0
            self.appearance = 220
            self.appearance_init = 0
            self.play_sound_sea_monster = True
        elif self.image == 'lightning':
            ### Load images
            self.obstacle_img1 = pygame.image.load(resource_path('images/lightning/lightning1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/lightning/lightning2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/lightning/lightning3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/lightning/lightning4.png')).convert_alpha()
            self.obstacle_img5 = pygame.image.load(resource_path('images/lightning/lightning5.png')).convert_alpha()
            self.obstacle_img6 = pygame.image.load(resource_path('images/lightning/lightning6.png')).convert_alpha()
            self.obstacle_img7 = pygame.image.load(resource_path('images/lightning/lightning7.png')).convert_alpha()
            self.obstacle_img8 = pygame.image.load(resource_path('images/lightning/lightning8.png')).convert_alpha()
            self.obstacle_img9 = pygame.image.load(resource_path('images/lightning/lightning9.png')).convert_alpha()
            self.obstacle_img10 = pygame.image.load(resource_path('images/lightning/lightning10.png')).convert_alpha()
            self.obstacle_img11 = pygame.image.load(resource_path('images/lightning/lightning11.png')).convert_alpha()
            self.obstacle_img12 = pygame.image.load(resource_path('images/lightning/lightning12.png')).convert_alpha()
            self.obstacle_img13 = pygame.image.load(resource_path('images/lightning/lightning13.png')).convert_alpha()
            self.obstacle_img14 = pygame.image.load(resource_path('images/lightning/lightning14.png')).convert_alpha()
            self.obstacle_img15 = pygame.image.load(resource_path('images/lightning/lightning15.png')).convert_alpha()
            self.obstacle_img16 = pygame.image.load(resource_path('images/lightning/lightning16.png')).convert_alpha()
            self.obstacle_img17 = pygame.image.load(resource_path('images/lightning/lightning17.png')).convert_alpha()
            self.obstacle_img18 = pygame.image.load(resource_path('images/lightning/lightning18.png')).convert_alpha()
            self.obstacle_img19 = pygame.image.load(resource_path('images/lightning/lightning19.png')).convert_alpha()
            self.obstacle_img20 = pygame.image.load(resource_path('images/lightning/lightning20.png')).convert_alpha()
            self.obstacle_img21 = pygame.image.load(resource_path('images/lightning/lightning21.png')).convert_alpha()
            self.obstacle_img22 = pygame.image.load(resource_path('images/lightning/lightning22.png')).convert_alpha()
            self.obstacle_img23 = pygame.image.load(resource_path('images/lightning/lightning23.png')).convert_alpha()
            self.obstacle_img24 = pygame.image.load(resource_path('images/lightning/lightning24.png')).convert_alpha()
            self.obstacle_img25 = pygame.image.load(resource_path('images/lightning/lightning25.png')).convert_alpha()
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            self.obstacle_images.append(self.obstacle_img5)
            self.obstacle_images.append(self.obstacle_img6)
            self.obstacle_images.append(self.obstacle_img7)
            self.obstacle_images.append(self.obstacle_img8)
            self.obstacle_images.append(self.obstacle_img9)
            self.obstacle_images.append(self.obstacle_img10)
            self.obstacle_images.append(self.obstacle_img11)
            self.obstacle_images.append(self.obstacle_img12)
            self.obstacle_images.append(self.obstacle_img13)
            self.obstacle_images.append(self.obstacle_img14)
            self.obstacle_images.append(self.obstacle_img15)
            self.obstacle_images.append(self.obstacle_img16)
            self.obstacle_images.append(self.obstacle_img17)
            self.obstacle_images.append(self.obstacle_img18)
            self.obstacle_images.append(self.obstacle_img19)
            self.obstacle_images.append(self.obstacle_img20)
            self.obstacle_images.append(self.obstacle_img21)
            self.obstacle_images.append(self.obstacle_img22)
            self.obstacle_images.append(self.obstacle_img23)
            self.obstacle_images.append(self.obstacle_img24)
            self.obstacle_images.append(self.obstacle_img25)
            ### Additional variables
            self.lightning_x_pos = randrange(10, W - 200)
            self.lightning_y_pos = 0
            self.obstacle_img = pygame.image.load(resource_path('images/lightning/lightning1.png')).convert_alpha()
            self.rect_lightning = self.obstacle_img.get_rect()
            self.rect_lightning.x = W
            self.rect_lightning.y = self.lightning_y_pos
            self.wait_lightning = 400  ### Cooldown for the lightning to appear
            self.lightning_appearance = 150
            self.lightning_appearance_init = 0
            self.play_sound_lightning = True
        elif self.image == 'raven_enemy1':
            self.obstacle_img1 = pygame.image.load(resource_path('images/raven/raven1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/raven/raven2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/raven/raven3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/raven/raven4.png')).convert_alpha()
            self.obstacle_img5 = pygame.image.load(resource_path('images/raven/raven5.png')).convert_alpha()
            self.obstacle_img6 = pygame.image.load(resource_path('images/raven/raven6.png')).convert_alpha()
            self.obstacle_img7 = pygame.image.load(resource_path('images/raven/raven7.png')).convert_alpha()
            self.obstacle_img8 = pygame.image.load(resource_path('images/raven/raven8.png')).convert_alpha()
            self.obstacle_img9 = pygame.image.load(resource_path('images/raven/raven9.png')).convert_alpha()
            self.obstacle_img10 = pygame.image.load(resource_path('images/raven/raven10.png')).convert_alpha()
            self.obstacle_img11 = pygame.image.load(resource_path('images/raven/raven11.png')).convert_alpha()
            self.obstacle_img12 = pygame.image.load(resource_path('images/raven/raven12.png')).convert_alpha()
            self.obstacle_img13 = pygame.image.load(resource_path('images/raven/raven13.png')).convert_alpha()
            self.obstacle_img14 = pygame.image.load(resource_path('images/raven/raven14.png')).convert_alpha()
            self.obstacle_img15 = pygame.image.load(resource_path('images/raven/raven15.png')).convert_alpha()
            self.obstacle_img16 = pygame.image.load(resource_path('images/raven/raven16.png')).convert_alpha()
            self.obstacle_img17 = pygame.image.load(resource_path('images/raven/raven17.png')).convert_alpha()
            self.obstacle_img18 = pygame.image.load(resource_path('images/raven/raven18.png')).convert_alpha()
            self.obstacle_img19 = pygame.image.load(resource_path('images/raven/raven19.png')).convert_alpha()
            self.obstacle_img20 = pygame.image.load(resource_path('images/raven/raven20.png')).convert_alpha()
            self.obstacle_img21 = pygame.image.load(resource_path('images/raven/raven21.png')).convert_alpha()
            self.obstacle_img22 = pygame.image.load(resource_path('images/raven/raven22.png')).convert_alpha()
            self.obstacle_img23 = pygame.image.load(resource_path('images/raven/raven23.png')).convert_alpha()
            self.obstacle_img1 = rotate_center(self.obstacle_img1, 40)
            self.obstacle_img2 = rotate_center(self.obstacle_img2, 40)
            self.obstacle_img3 = rotate_center(self.obstacle_img3, 40)
            self.obstacle_img4 = rotate_center(self.obstacle_img4, 40)
            self.obstacle_img5 = rotate_center(self.obstacle_img5, 40)
            self.obstacle_img6 = rotate_center(self.obstacle_img6, 40)
            self.obstacle_img7 = rotate_center(self.obstacle_img7, 40)
            self.obstacle_img8 = rotate_center(self.obstacle_img8, 40)
            self.obstacle_img9 = rotate_center(self.obstacle_img9, 40)
            self.obstacle_img10 = rotate_center(self.obstacle_img10, 40)
            self.obstacle_img11 = rotate_center(self.obstacle_img11, 40)
            self.obstacle_img12 = rotate_center(self.obstacle_img12, 40)
            self.obstacle_img13 = rotate_center(self.obstacle_img13, 40)
            self.obstacle_img14 = rotate_center(self.obstacle_img14, 40)
            self.obstacle_img15 = rotate_center(self.obstacle_img15, 40)
            self.obstacle_img16 = rotate_center(self.obstacle_img16, 40)
            self.obstacle_img17 = rotate_center(self.obstacle_img17, 40)
            self.obstacle_img18 = rotate_center(self.obstacle_img18, 40)
            self.obstacle_img19 = rotate_center(self.obstacle_img19, 40)
            self.obstacle_img20 = rotate_center(self.obstacle_img20, 40)
            self.obstacle_img21 = rotate_center(self.obstacle_img21, 40)
            self.obstacle_img22 = rotate_center(self.obstacle_img22, 40)
            self.obstacle_img23 = rotate_center(self.obstacle_img23, 40)
            self.obstacle_img1 = pygame.transform.scale(self.obstacle_img1, (75, 75))
            self.obstacle_img2 = pygame.transform.scale(self.obstacle_img2, (75, 75))
            self.obstacle_img3 = pygame.transform.scale(self.obstacle_img3, (75, 75))
            self.obstacle_img4 = pygame.transform.scale(self.obstacle_img4, (75, 75))
            self.obstacle_img5 = pygame.transform.scale(self.obstacle_img5, (75, 75))
            self.obstacle_img6 = pygame.transform.scale(self.obstacle_img6, (75, 75))
            self.obstacle_img7 = pygame.transform.scale(self.obstacle_img7, (75, 75))
            self.obstacle_img8 = pygame.transform.scale(self.obstacle_img8, (75, 75))
            self.obstacle_img9 = pygame.transform.scale(self.obstacle_img9, (75, 75))
            self.obstacle_img10 = pygame.transform.scale(self.obstacle_img10, (75, 75))
            self.obstacle_img11 = pygame.transform.scale(self.obstacle_img11, (75, 75))
            self.obstacle_img12 = pygame.transform.scale(self.obstacle_img12, (75, 75))
            self.obstacle_img13 = pygame.transform.scale(self.obstacle_img13, (75, 75))
            self.obstacle_img14 = pygame.transform.scale(self.obstacle_img14, (75, 75))
            self.obstacle_img15 = pygame.transform.scale(self.obstacle_img15, (75, 75))
            self.obstacle_img16 = pygame.transform.scale(self.obstacle_img16, (75, 75))
            self.obstacle_img17 = pygame.transform.scale(self.obstacle_img17, (75, 75))
            self.obstacle_img18 = pygame.transform.scale(self.obstacle_img18, (75, 75))
            self.obstacle_img19 = pygame.transform.scale(self.obstacle_img19, (75, 75))
            self.obstacle_img20 = pygame.transform.scale(self.obstacle_img20, (75, 75))
            self.obstacle_img21 = pygame.transform.scale(self.obstacle_img21, (75, 75))
            self.obstacle_img22 = pygame.transform.scale(self.obstacle_img22, (75, 75))
            self.obstacle_img23 = pygame.transform.scale(self.obstacle_img23, (75, 75))
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            self.obstacle_images.append(self.obstacle_img5)
            self.obstacle_images.append(self.obstacle_img6)
            self.obstacle_images.append(self.obstacle_img7)
            self.obstacle_images.append(self.obstacle_img8)
            self.obstacle_images.append(self.obstacle_img9)
            self.obstacle_images.append(self.obstacle_img10)
            self.obstacle_images.append(self.obstacle_img11)
            self.obstacle_images.append(self.obstacle_img12)
            self.obstacle_images.append(self.obstacle_img13)
            self.obstacle_images.append(self.obstacle_img14)
            self.obstacle_images.append(self.obstacle_img15)
            self.obstacle_images.append(self.obstacle_img16)
            self.obstacle_images.append(self.obstacle_img17)
            self.obstacle_images.append(self.obstacle_img18)
            self.obstacle_images.append(self.obstacle_img19)
            self.obstacle_images.append(self.obstacle_img20)
            self.obstacle_images.append(self.obstacle_img21)
            self.obstacle_images.append(self.obstacle_img22)
            self.obstacle_images.append(self.obstacle_img23)
            ### Additional variables
            self.raven_enemy1_x_pos = randrange(W // 2, W)
            self.raven_enemy1_y_pos = 0
            self.obstacle_img = pygame.image.load(resource_path('images/raven/raven1.png')).convert_alpha()
            self.rect_raven_enemy1 = self.obstacle_img.get_rect()
            self.rect_raven_enemy1.x = self.raven_enemy1_x_pos + 25
            self.rect_raven_enemy1.y = self.raven_enemy1_y_pos + 15
            self.play_sound_raven_enemy1 = True
        '''
        elif self.image == 'raven':
            self.obstacle_img1 = pygame.image.load(resource_path('images/raven/raven1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/raven/raven2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/raven/raven3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/raven/raven4.png')).convert_alpha()
            self.obstacle_img5 = pygame.image.load(resource_path('images/raven/raven5.png')).convert_alpha()
            self.obstacle_img6 = pygame.image.load(resource_path('images/raven/raven6.png')).convert_alpha()
            self.obstacle_img7 = pygame.image.load(resource_path('images/raven/raven7.png')).convert_alpha()
            self.obstacle_img8 = pygame.image.load(resource_path('images/raven/raven8.png')).convert_alpha()
            self.obstacle_img9 = pygame.image.load(resource_path('images/raven/raven9.png')).convert_alpha()
            self.obstacle_img10 = pygame.image.load(resource_path('images/raven/raven10.png')).convert_alpha()
            self.obstacle_img11 = pygame.image.load(resource_path('images/raven/raven11.png')).convert_alpha()
            self.obstacle_img12 = pygame.image.load(resource_path('images/raven/raven12.png')).convert_alpha()
            self.obstacle_img13 = pygame.image.load(resource_path('images/raven/raven13.png')).convert_alpha()
            self.obstacle_img14 = pygame.image.load(resource_path('images/raven/raven14.png')).convert_alpha()
            self.obstacle_img15 = pygame.image.load(resource_path('images/raven/raven15.png')).convert_alpha()
            self.obstacle_img16 = pygame.image.load(resource_path('images/raven/raven16.png')).convert_alpha()
            self.obstacle_img17 = pygame.image.load(resource_path('images/raven/raven17.png')).convert_alpha()
            self.obstacle_img18 = pygame.image.load(resource_path('images/raven/raven18.png')).convert_alpha()
            self.obstacle_img19 = pygame.image.load(resource_path('images/raven/raven19.png')).convert_alpha()
            self.obstacle_img20 = pygame.image.load(resource_path('images/raven/raven20.png')).convert_alpha()
            self.obstacle_img21 = pygame.image.load(resource_path('images/raven/raven21.png')).convert_alpha()
            self.obstacle_img22 = pygame.image.load(resource_path('images/raven/raven22.png')).convert_alpha()
            self.obstacle_img23 = pygame.image.load(resource_path('images/raven/raven23.png')).convert_alpha()
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            self.obstacle_images.append(self.obstacle_img5)
            self.obstacle_images.append(self.obstacle_img6)
            self.obstacle_images.append(self.obstacle_img7)
            self.obstacle_images.append(self.obstacle_img8)
            self.obstacle_images.append(self.obstacle_img9)
            self.obstacle_images.append(self.obstacle_img10)
            self.obstacle_images.append(self.obstacle_img11)
            self.obstacle_images.append(self.obstacle_img12)
            self.obstacle_images.append(self.obstacle_img13)
            self.obstacle_images.append(self.obstacle_img14)
            self.obstacle_images.append(self.obstacle_img15)
            self.obstacle_images.append(self.obstacle_img16)
            self.obstacle_images.append(self.obstacle_img17)
            self.obstacle_images.append(self.obstacle_img18)
            self.obstacle_images.append(self.obstacle_img19)
            self.obstacle_images.append(self.obstacle_img20)
            self.obstacle_images.append(self.obstacle_img21)
            self.obstacle_images.append(self.obstacle_img22)
            self.obstacle_images.append(self.obstacle_img23)
        elif self.image == 'raven2':
            self.obstacle_img1 = pygame.image.load(resource_path('images/raven/raven1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/raven/raven2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/raven/raven3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/raven/raven4.png')).convert_alpha()
            self.obstacle_img5 = pygame.image.load(resource_path('images/raven/raven5.png')).convert_alpha()
            self.obstacle_img6 = pygame.image.load(resource_path('images/raven/raven6.png')).convert_alpha()
            self.obstacle_img7 = pygame.image.load(resource_path('images/raven/raven7.png')).convert_alpha()
            self.obstacle_img8 = pygame.image.load(resource_path('images/raven/raven8.png')).convert_alpha()
            self.obstacle_img9 = pygame.image.load(resource_path('images/raven/raven9.png')).convert_alpha()
            self.obstacle_img10 = pygame.image.load(resource_path('images/raven/raven10.png')).convert_alpha()
            self.obstacle_img11 = pygame.image.load(resource_path('images/raven/raven11.png')).convert_alpha()
            self.obstacle_img12 = pygame.image.load(resource_path('images/raven/raven12.png')).convert_alpha()
            self.obstacle_img13 = pygame.image.load(resource_path('images/raven/raven13.png')).convert_alpha()
            self.obstacle_img14 = pygame.image.load(resource_path('images/raven/raven14.png')).convert_alpha()
            self.obstacle_img15 = pygame.image.load(resource_path('images/raven/raven15.png')).convert_alpha()
            self.obstacle_img16 = pygame.image.load(resource_path('images/raven/raven16.png')).convert_alpha()
            self.obstacle_img17 = pygame.image.load(resource_path('images/raven/raven17.png')).convert_alpha()
            self.obstacle_img18 = pygame.image.load(resource_path('images/raven/raven18.png')).convert_alpha()
            self.obstacle_img19 = pygame.image.load(resource_path('images/raven/raven19.png')).convert_alpha()
            self.obstacle_img20 = pygame.image.load(resource_path('images/raven/raven20.png')).convert_alpha()
            self.obstacle_img21 = pygame.image.load(resource_path('images/raven/raven21.png')).convert_alpha()
            self.obstacle_img22 = pygame.image.load(resource_path('images/raven/raven22.png')).convert_alpha()
            self.obstacle_img23 = pygame.image.load(resource_path('images/raven/raven23.png')).convert_alpha()
            self.obstacle_img1 = pygame.transform.flip(self.obstacle_img1, True, False)
            self.obstacle_img2 = pygame.transform.flip(self.obstacle_img2, True, False)
            self.obstacle_img3 = pygame.transform.flip(self.obstacle_img3, True, False)
            self.obstacle_img4 = pygame.transform.flip(self.obstacle_img4, True, False)
            self.obstacle_img5 = pygame.transform.flip(self.obstacle_img5, True, False)
            self.obstacle_img6 = pygame.transform.flip(self.obstacle_img6, True, False)
            self.obstacle_img7 = pygame.transform.flip(self.obstacle_img7, True, False)
            self.obstacle_img8 = pygame.transform.flip(self.obstacle_img8, True, False)
            self.obstacle_img9 = pygame.transform.flip(self.obstacle_img9, True, False)
            self.obstacle_img10 = pygame.transform.flip(self.obstacle_img10, True, False)
            self.obstacle_img11 = pygame.transform.flip(self.obstacle_img11, True, False)
            self.obstacle_img12 = pygame.transform.flip(self.obstacle_img12, True, False)
            self.obstacle_img13 = pygame.transform.flip(self.obstacle_img13, True, False)
            self.obstacle_img14 = pygame.transform.flip(self.obstacle_img14, True, False)
            self.obstacle_img15 = pygame.transform.flip(self.obstacle_img15, True, False)
            self.obstacle_img16 = pygame.transform.flip(self.obstacle_img16, True, False)
            self.obstacle_img17 = pygame.transform.flip(self.obstacle_img17, True, False)
            self.obstacle_img18 = pygame.transform.flip(self.obstacle_img18, True, False)
            self.obstacle_img19 = pygame.transform.flip(self.obstacle_img19, True, False)
            self.obstacle_img20 = pygame.transform.flip(self.obstacle_img20, True, False)
            self.obstacle_img21 = pygame.transform.flip(self.obstacle_img21, True, False)
            self.obstacle_img22 = pygame.transform.flip(self.obstacle_img22, True, False)
            self.obstacle_img23 = pygame.transform.flip(self.obstacle_img23, True, False)
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            self.obstacle_images.append(self.obstacle_img5)
            self.obstacle_images.append(self.obstacle_img6)
            self.obstacle_images.append(self.obstacle_img7)
            self.obstacle_images.append(self.obstacle_img8)
            self.obstacle_images.append(self.obstacle_img9)
            self.obstacle_images.append(self.obstacle_img10)
            self.obstacle_images.append(self.obstacle_img11)
            self.obstacle_images.append(self.obstacle_img12)
            self.obstacle_images.append(self.obstacle_img13)
            self.obstacle_images.append(self.obstacle_img14)
            self.obstacle_images.append(self.obstacle_img15)
            self.obstacle_images.append(self.obstacle_img16)
            self.obstacle_images.append(self.obstacle_img17)
            self.obstacle_images.append(self.obstacle_img18)
            self.obstacle_images.append(self.obstacle_img19)
            self.obstacle_images.append(self.obstacle_img20)
            self.obstacle_images.append(self.obstacle_img21)
            self.obstacle_images.append(self.obstacle_img22)
            self.obstacle_images.append(self.obstacle_img23)
        elif self.image == 'raven_enemy2':
            self.obstacle_img1 = pygame.image.load(resource_path('images/raven/raven1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/raven/raven2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/raven/raven3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/raven/raven4.png')).convert_alpha()
            self.obstacle_img5 = pygame.image.load(resource_path('images/raven/raven5.png')).convert_alpha()
            self.obstacle_img6 = pygame.image.load(resource_path('images/raven/raven6.png')).convert_alpha()
            self.obstacle_img7 = pygame.image.load(resource_path('images/raven/raven7.png')).convert_alpha()
            self.obstacle_img8 = pygame.image.load(resource_path('images/raven/raven8.png')).convert_alpha()
            self.obstacle_img9 = pygame.image.load(resource_path('images/raven/raven9.png')).convert_alpha()
            self.obstacle_img10 = pygame.image.load(resource_path('images/raven/raven10.png')).convert_alpha()
            self.obstacle_img11 = pygame.image.load(resource_path('images/raven/raven11.png')).convert_alpha()
            self.obstacle_img12 = pygame.image.load(resource_path('images/raven/raven12.png')).convert_alpha()
            self.obstacle_img13 = pygame.image.load(resource_path('images/raven/raven13.png')).convert_alpha()
            self.obstacle_img14 = pygame.image.load(resource_path('images/raven/raven14.png')).convert_alpha()
            self.obstacle_img15 = pygame.image.load(resource_path('images/raven/raven15.png')).convert_alpha()
            self.obstacle_img16 = pygame.image.load(resource_path('images/raven/raven16.png')).convert_alpha()
            self.obstacle_img17 = pygame.image.load(resource_path('images/raven/raven17.png')).convert_alpha()
            self.obstacle_img18 = pygame.image.load(resource_path('images/raven/raven18.png')).convert_alpha()
            self.obstacle_img19 = pygame.image.load(resource_path('images/raven/raven19.png')).convert_alpha()
            self.obstacle_img20 = pygame.image.load(resource_path('images/raven/raven20.png')).convert_alpha()
            self.obstacle_img21 = pygame.image.load(resource_path('images/raven/raven21.png')).convert_alpha()
            self.obstacle_img22 = pygame.image.load(resource_path('images/raven/raven22.png')).convert_alpha()
            self.obstacle_img23 = pygame.image.load(resource_path('images/raven/raven23.png')).convert_alpha()
            self.obstacle_img1 = rotate_center(self.obstacle_img1, 40)
            self.obstacle_img2 = rotate_center(self.obstacle_img2, 40)
            self.obstacle_img3 = rotate_center(self.obstacle_img3, 40)
            self.obstacle_img4 = rotate_center(self.obstacle_img4, 40)
            self.obstacle_img5 = rotate_center(self.obstacle_img5, 40)
            self.obstacle_img6 = rotate_center(self.obstacle_img6, 40)
            self.obstacle_img7 = rotate_center(self.obstacle_img7, 40)
            self.obstacle_img8 = rotate_center(self.obstacle_img8, 40)
            self.obstacle_img9 = rotate_center(self.obstacle_img9, 40)
            self.obstacle_img10 = rotate_center(self.obstacle_img10, 40)
            self.obstacle_img11 = rotate_center(self.obstacle_img11, 40)
            self.obstacle_img12 = rotate_center(self.obstacle_img12, 40)
            self.obstacle_img13 = rotate_center(self.obstacle_img13, 40)
            self.obstacle_img14 = rotate_center(self.obstacle_img14, 40)
            self.obstacle_img15 = rotate_center(self.obstacle_img15, 40)
            self.obstacle_img16 = rotate_center(self.obstacle_img16, 40)
            self.obstacle_img17 = rotate_center(self.obstacle_img17, 40)
            self.obstacle_img18 = rotate_center(self.obstacle_img18, 40)
            self.obstacle_img19 = rotate_center(self.obstacle_img19, 40)
            self.obstacle_img20 = rotate_center(self.obstacle_img20, 40)
            self.obstacle_img21 = rotate_center(self.obstacle_img21, 40)
            self.obstacle_img22 = rotate_center(self.obstacle_img22, 40)
            self.obstacle_img23 = rotate_center(self.obstacle_img23, 40)
            self.obstacle_img1 = pygame.transform.scale(self.obstacle_img1, (75, 75))
            self.obstacle_img2 = pygame.transform.scale(self.obstacle_img2, (75, 75))
            self.obstacle_img3 = pygame.transform.scale(self.obstacle_img3, (75, 75))
            self.obstacle_img4 = pygame.transform.scale(self.obstacle_img4, (75, 75))
            self.obstacle_img5 = pygame.transform.scale(self.obstacle_img5, (75, 75))
            self.obstacle_img6 = pygame.transform.scale(self.obstacle_img6, (75, 75))
            self.obstacle_img7 = pygame.transform.scale(self.obstacle_img7, (75, 75))
            self.obstacle_img8 = pygame.transform.scale(self.obstacle_img8, (75, 75))
            self.obstacle_img9 = pygame.transform.scale(self.obstacle_img9, (75, 75))
            self.obstacle_img10 = pygame.transform.scale(self.obstacle_img10, (75, 75))
            self.obstacle_img11 = pygame.transform.scale(self.obstacle_img11, (75, 75))
            self.obstacle_img12 = pygame.transform.scale(self.obstacle_img12, (75, 75))
            self.obstacle_img13 = pygame.transform.scale(self.obstacle_img13, (75, 75))
            self.obstacle_img14 = pygame.transform.scale(self.obstacle_img14, (75, 75))
            self.obstacle_img15 = pygame.transform.scale(self.obstacle_img15, (75, 75))
            self.obstacle_img16 = pygame.transform.scale(self.obstacle_img16, (75, 75))
            self.obstacle_img17 = pygame.transform.scale(self.obstacle_img17, (75, 75))
            self.obstacle_img18 = pygame.transform.scale(self.obstacle_img18, (75, 75))
            self.obstacle_img19 = pygame.transform.scale(self.obstacle_img19, (75, 75))
            self.obstacle_img20 = pygame.transform.scale(self.obstacle_img20, (75, 75))
            self.obstacle_img21 = pygame.transform.scale(self.obstacle_img21, (75, 75))
            self.obstacle_img22 = pygame.transform.scale(self.obstacle_img22, (75, 75))
            self.obstacle_img23 = pygame.transform.scale(self.obstacle_img23, (75, 75))
            self.obstacle_img1 = pygame.transform.flip(self.obstacle_img1, True, False)
            self.obstacle_img2 = pygame.transform.flip(self.obstacle_img2, True, False)
            self.obstacle_img3 = pygame.transform.flip(self.obstacle_img3, True, False)
            self.obstacle_img4 = pygame.transform.flip(self.obstacle_img4, True, False)
            self.obstacle_img5 = pygame.transform.flip(self.obstacle_img5, True, False)
            self.obstacle_img6 = pygame.transform.flip(self.obstacle_img6, True, False)
            self.obstacle_img7 = pygame.transform.flip(self.obstacle_img7, True, False)
            self.obstacle_img8 = pygame.transform.flip(self.obstacle_img8, True, False)
            self.obstacle_img9 = pygame.transform.flip(self.obstacle_img9, True, False)
            self.obstacle_img10 = pygame.transform.flip(self.obstacle_img10, True, False)
            self.obstacle_img11 = pygame.transform.flip(self.obstacle_img11, True, False)
            self.obstacle_img12 = pygame.transform.flip(self.obstacle_img12, True, False)
            self.obstacle_img13 = pygame.transform.flip(self.obstacle_img13, True, False)
            self.obstacle_img14 = pygame.transform.flip(self.obstacle_img14, True, False)
            self.obstacle_img15 = pygame.transform.flip(self.obstacle_img15, True, False)
            self.obstacle_img16 = pygame.transform.flip(self.obstacle_img16, True, False)
            self.obstacle_img17 = pygame.transform.flip(self.obstacle_img17, True, False)
            self.obstacle_img18 = pygame.transform.flip(self.obstacle_img18, True, False)
            self.obstacle_img19 = pygame.transform.flip(self.obstacle_img19, True, False)
            self.obstacle_img20 = pygame.transform.flip(self.obstacle_img20, True, False)
            self.obstacle_img21 = pygame.transform.flip(self.obstacle_img21, True, False)
            self.obstacle_img22 = pygame.transform.flip(self.obstacle_img22, True, False)
            self.obstacle_img23 = pygame.transform.flip(self.obstacle_img23, True, False)
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            self.obstacle_images.append(self.obstacle_img5)
            self.obstacle_images.append(self.obstacle_img6)
            self.obstacle_images.append(self.obstacle_img7)
            self.obstacle_images.append(self.obstacle_img8)
            self.obstacle_images.append(self.obstacle_img9)
            self.obstacle_images.append(self.obstacle_img10)
            self.obstacle_images.append(self.obstacle_img11)
            self.obstacle_images.append(self.obstacle_img12)
            self.obstacle_images.append(self.obstacle_img13)
            self.obstacle_images.append(self.obstacle_img14)
            self.obstacle_images.append(self.obstacle_img15)
            self.obstacle_images.append(self.obstacle_img16)
            self.obstacle_images.append(self.obstacle_img17)
            self.obstacle_images.append(self.obstacle_img18)
            self.obstacle_images.append(self.obstacle_img19)
            self.obstacle_images.append(self.obstacle_img20)
            self.obstacle_images.append(self.obstacle_img21)
            self.obstacle_images.append(self.obstacle_img22)
            self.obstacle_images.append(self.obstacle_img23)
            ### Additional variables
            self.raven_enemy2_x_pos = randrange(0, W // 2)
            self.raven_enemy2_y_pos = 0
            self.obstacle_img = pygame.image.load(resource_path('images/raven/raven1.png')).convert_alpha()
            self.rect_raven_enemy2 = self.obstacle_img.get_rect()
            self.rect_raven_enemy2.x = self.raven_enemy2_x_pos + 15
            self.rect_raven_enemy2.y = self.raven_enemy2_y_pos + 25
            self.play_sound_raven_enemy2 = True
        elif self.image == 'sleipnir':
            self.obstacle_img1 = pygame.image.load(resource_path('images/sleipnir/sleipnir1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/sleipnir/sleipnir2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/sleipnir/sleipnir3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/sleipnir/sleipnir4.png')).convert_alpha()
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            self.obstacle_img1_flipped = pygame.transform.flip(self.obstacle_img1, True, False)
            self.obstacle_img2_flipped = pygame.transform.flip(self.obstacle_img2, True, False)
            self.obstacle_img3_flipped = pygame.transform.flip(self.obstacle_img3, True, False)
            self.obstacle_img4_flipped = pygame.transform.flip(self.obstacle_img4, True, False)
            self.obstacle_images_flipped = []
            self.obstacle_images_flipped.append(self.obstacle_img1_flipped)
            self.obstacle_images_flipped.append(self.obstacle_img2_flipped)
            self.obstacle_images_flipped.append(self.obstacle_img3_flipped)
            self.obstacle_images_flipped.append(self.obstacle_img4_flipped)
        elif self.image == 'viking1' or self.image == 'viking2' or self.image == 'viking3' or self.image == 'viking4' or self.image == 'viking5':
            self.obstacle_img1 = pygame.image.load(resource_path('images/viking/viking1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/viking/viking2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/viking/viking3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/viking/viking4.png')).convert_alpha()
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
        elif self.image == 'viking_ship':
            ### Load images
            self.obstacle_img1 = pygame.image.load(resource_path('images/viking_ship/viking_ship1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/viking_ship/viking_ship2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/viking_ship/viking_ship3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/viking_ship/viking_ship4.png')).convert_alpha()
            self.obstacle_img5 = pygame.image.load(resource_path('images/viking_ship/viking_ship5.png')).convert_alpha()
            self.obstacle_img6 = pygame.image.load(resource_path('images/viking_ship/viking_ship6.png')).convert_alpha()
            self.obstacle_img7 = pygame.image.load(resource_path('images/viking_ship/viking_ship7.png')).convert_alpha()
            self.obstacle_img8 = pygame.image.load(resource_path('images/viking_ship/viking_ship8.png')).convert_alpha()
            self.obstacle_img9 = pygame.image.load(resource_path('images/viking_ship/viking_ship9.png')).convert_alpha()
            self.obstacle_img10 = pygame.image.load(resource_path('images/viking_ship/viking_ship10.png')).convert_alpha()
            self.obstacle_img11 = pygame.image.load(resource_path('images/viking_ship/viking_ship11.png')).convert_alpha()
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            self.obstacle_images.append(self.obstacle_img5)
            self.obstacle_images.append(self.obstacle_img6)
            self.obstacle_images.append(self.obstacle_img7)
            self.obstacle_images.append(self.obstacle_img8)
            self.obstacle_images.append(self.obstacle_img9)
            self.obstacle_images.append(self.obstacle_img10)
            self.obstacle_images.append(self.obstacle_img11)
            ### Additional variables
            self.viking_ship_x_pos = W
            self.viking_ship_y_pos = randrange(H // 3, int(H * 0.95))
            self.obstacle_img = pygame.image.load(resource_path('images/viking_ship/viking_ship1.png')).convert_alpha()
            self.rect_viking_ship = self.obstacle_img.get_rect()
            self.rect_viking_ship.x = W + 20
            self.rect_viking_ship.y = self.viking_ship_y_pos + 30
            self.play_sound_viking_ship = True
        elif self.image == 'flying_object':
            ### Load images
            self.obstacle_img1 = pygame.image.load(resource_path('images/flying_object/flying_object1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/flying_object/flying_object2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/flying_object/flying_object3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/flying_object/flying_object4.png')).convert_alpha()
            self.obstacle_img5 = pygame.image.load(resource_path('images/flying_object/flying_object5.png')).convert_alpha()
            self.obstacle_img6 = pygame.image.load(resource_path('images/flying_object/flying_object6.png')).convert_alpha()
            self.obstacle_img7 = pygame.image.load(resource_path('images/flying_object/flying_object7.png')).convert_alpha()
            self.obstacle_img8 = pygame.image.load(resource_path('images/flying_object/flying_object8.png')).convert_alpha()
            self.obstacle_img9 = pygame.image.load(resource_path('images/flying_object/flying_object9.png')).convert_alpha()
            self.obstacle_img10 = pygame.image.load(resource_path('images/flying_object/flying_object10.png')).convert_alpha()
            self.obstacle_img11 = pygame.image.load(resource_path('images/flying_object/flying_object11.png')).convert_alpha()
            self.obstacle_img12 = pygame.image.load(resource_path('images/flying_object/flying_object12.png')).convert_alpha()
            self.obstacle_img13 = pygame.image.load(resource_path('images/flying_object/flying_object13.png')).convert_alpha()
            self.obstacle_img14 = pygame.image.load(resource_path('images/flying_object/flying_object14.png')).convert_alpha()
            self.obstacle_img15 = pygame.image.load(resource_path('images/flying_object/flying_object15.png')).convert_alpha()
            self.obstacle_img16 = pygame.image.load(resource_path('images/flying_object/flying_object16.png')).convert_alpha()
            self.obstacle_img17 = pygame.image.load(resource_path('images/flying_object/flying_object17.png')).convert_alpha()
            self.obstacle_img18 = pygame.image.load(resource_path('images/flying_object/flying_object18.png')).convert_alpha()
            self.obstacle_img19 = pygame.image.load(resource_path('images/flying_object/flying_object19.png')).convert_alpha()
            self.obstacle_img20 = pygame.image.load(resource_path('images/flying_object/flying_object20.png')).convert_alpha()
            self.obstacle_img21 = pygame.image.load(resource_path('images/flying_object/flying_object21.png')).convert_alpha()
            self.obstacle_img22 = pygame.image.load(resource_path('images/flying_object/flying_object22.png')).convert_alpha()
            self.obstacle_img23 = pygame.image.load(resource_path('images/flying_object/flying_object23.png')).convert_alpha()
            self.obstacle_img24 = pygame.image.load(resource_path('images/flying_object/flying_object24.png')).convert_alpha()
            self.obstacle_img25 = pygame.image.load(resource_path('images/flying_object/flying_object25.png')).convert_alpha()
            self.obstacle_img26 = pygame.image.load(resource_path('images/flying_object/flying_object26.png')).convert_alpha()
            self.obstacle_img27 = pygame.image.load(resource_path('images/flying_object/flying_object27.png')).convert_alpha()
            self.obstacle_img28 = pygame.image.load(resource_path('images/flying_object/flying_object28.png')).convert_alpha()
            self.obstacle_img29 = pygame.image.load(resource_path('images/flying_object/flying_object29.png')).convert_alpha()
            self.obstacle_img30 = pygame.image.load(resource_path('images/flying_object/flying_object30.png')).convert_alpha()
            ### Additional variables
            self.flying_object_x_pos = randrange(10, W - 200)
            self.flying_object_y_pos = -random.randrange(400)
            self.obstacle_img = pygame.image.load(resource_path('images/flying_object/flying_object1.png')).convert_alpha()
            self.rect_flying_object = self.obstacle_img.get_rect()
            self.rect_flying_object.x = W
            self.rect_flying_object.y = self.flying_object_y_pos
            self.wait_flying_object = 400  ### Cooldown for the flying object to appear
            self.flying_object_appearance = 115
            self.flying_object_appearance_init = 0
            self.play_sound_flying_object = True

            #self.obstacle_img1 = pygame.transform.scale(self.obstacle_img1, (230, 120))
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            self.obstacle_images.append(self.obstacle_img5)
            self.obstacle_images.append(self.obstacle_img6)
            self.obstacle_images.append(self.obstacle_img7)
            self.obstacle_images.append(self.obstacle_img8)
            self.obstacle_images.append(self.obstacle_img9)
            self.obstacle_images.append(self.obstacle_img10)
            self.obstacle_images.append(self.obstacle_img11)
            self.obstacle_images.append(self.obstacle_img12)
            self.obstacle_images.append(self.obstacle_img13)
            self.obstacle_images.append(self.obstacle_img14)
            self.obstacle_images.append(self.obstacle_img15)
            self.obstacle_images.append(self.obstacle_img16)
            self.obstacle_images.append(self.obstacle_img17)
            self.obstacle_images.append(self.obstacle_img18)
            self.obstacle_images.append(self.obstacle_img19)
            self.obstacle_images.append(self.obstacle_img20)
            self.obstacle_images.append(self.obstacle_img21)
            self.obstacle_images.append(self.obstacle_img22)
            self.obstacle_images.append(self.obstacle_img23)
            self.obstacle_images.append(self.obstacle_img24)
            self.obstacle_images.append(self.obstacle_img25)
            self.obstacle_images.append(self.obstacle_img26)
            self.obstacle_images.append(self.obstacle_img27)
            self.obstacle_images.append(self.obstacle_img28)
            self.obstacle_images.append(self.obstacle_img29)
            self.obstacle_images.append(self.obstacle_img30)
        elif self.image == 'lightning_background':
            ### Load images
            self.obstacle_img1 = pygame.image.load(resource_path('images/lightning/lightning_background1.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/lightning/lightning_background2.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/lightning/lightning_background3.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/lightning/lightning_background4.png')).convert_alpha()
            self.obstacle_img5 = pygame.image.load(resource_path('images/lightning/lightning_background5.png')).convert_alpha()
            self.obstacle_img6 = pygame.image.load(resource_path('images/lightning/lightning_background6.png')).convert_alpha()
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            self.obstacle_images.append(self.obstacle_img5)
            self.obstacle_images.append(self.obstacle_img6)
            ### Additional variables
            self.lightning_background_x_pos = randrange(10, W - 200)
            self.lightning_background_y_pos = 0
            self.obstacle_img = pygame.image.load(resource_path('images/lightning/lightning_background1.png')).convert_alpha()
            self.wait_lightning_background = 300  ### Cooldown for the lightning to appear
            self.lightning_background_appearance = 35
            self.lightning_background_appearance_init = 0
            self.play_sound_lightning = True
        elif self.image == 'sleipnir_enemy':
            ### Load images
            self.obstacle_img1 = pygame.image.load(resource_path('images/sleipnir/sleipnir1_enemy.png')).convert_alpha()
            self.obstacle_img2 = pygame.image.load(resource_path('images/sleipnir/sleipnir2_enemy.png')).convert_alpha()
            self.obstacle_img3 = pygame.image.load(resource_path('images/sleipnir/sleipnir3_enemy.png')).convert_alpha()
            self.obstacle_img4 = pygame.image.load(resource_path('images/sleipnir/sleipnir4_enemy.png')).convert_alpha()
            self.obstacle_images = []
            self.obstacle_images.append(self.obstacle_img1)
            self.obstacle_images.append(self.obstacle_img2)
            self.obstacle_images.append(self.obstacle_img3)
            self.obstacle_images.append(self.obstacle_img4)
            ### Additional variables
            self.sleipnir_enemy_x_pos = W
            self.sleipnir_enemy_y_pos = randrange(H // 3, int(H * 0.95))
            self.obstacle_img = pygame.image.load(resource_path('images/sleipnir/sleipnir1_enemy.png')).convert_alpha()
            self.rect_sleipnir_enemy = self.obstacle_img.get_rect()
            self.rect_sleipnir_enemy.x = W
            self.rect_sleipnir_enemy.y = self.sleipnir_enemy_y_pos
            self.play_sound_sleipnir_enemy = True
        '''

    def add_obstacle_at_location(self):
        '''
        if self.image == 'fireball':
            self.obstacle_img = self.obstacle_images[self.index]
            screen.blit(self.obstacle_img, (self.obstacle_x_pos, self.obstacle_y_pos))
            self.obstacle_x_pos -= self.obstacle_speed
            self.rect_fireball.x -= self.obstacle_speed

            if self.obstacle_x_pos <= -50:
                self.obstacle_x_pos = W
                self.obstacle_y_pos = randrange(H // 3, int(H * 0.95))
                self.rect_fireball.x = W
                self.rect_fireball.y = self.obstacle_y_pos
                self.play_sound_fire = True

            if self.ANIMATION_COOLDOWN > 30:
                self.index += 1
                self.ANIMATION_COOLDOWN = 0
            if self.index >= len(self.obstacle_images):
                self.index = 0
            self.obstacle_img = self.obstacle_images[self.index]
            self.ANIMATION_COOLDOWN += 10
        '''

        if self.image == 'rock':
            screen.blit(self.obstacle_img, (self.obstacle_x_pos, self.obstacle_y_pos))
            self.obstacle_x_pos -= self.obstacle_speed
            self.rect_rock.x -= self.obstacle_speed
            #pygame.draw.rect(screen, 'red',(self.rect_rock.x, self.rect_rock.y, self.rect_rock.width, self.rect_rock.height), 2)

            if self.obstacle_x_pos <= -50:
                self.obstacle_x_pos = W
                self.obstacle_y_pos = randrange(H // 3, int(H * 0.95))
                self.rect_rock.x = W
                self.rect_rock.y = self.obstacle_y_pos

        elif self.image == 'axe':
            self.obstacle_img = self.obstacle_images[self.index]
            screen.blit(self.obstacle_img, (self.obstacle_x_pos, self.obstacle_y_pos))
            self.obstacle_x_pos -= self.obstacle_speed
            self.rect_axe.x -= self.obstacle_speed

            if self.obstacle_x_pos <= -500:
                self.obstacle_x_pos = W
                self.obstacle_y_pos = randrange(H // 3, int(H * 0.95))
                self.rect_axe.x = W
                self.rect_axe.y = self.obstacle_y_pos
                self.play_sound_axe = True

            if self.ANIMATION_COOLDOWN > 50:
                self.index += 1
                self.ANIMATION_COOLDOWN = 0
            if self.index >= len(self.obstacle_images):
                self.index = 0
            self.obstacle_img = self.obstacle_images[self.index]
            self.ANIMATION_COOLDOWN += 10

        elif self.image == 'sea_monster':
            self.obstacle_img = self.obstacle_images[self.index]

            if self.init_wait == self.wait_sea_monster:
                screen.blit(self.obstacle_img, (self.sea_monster_x_pos, self.sea_monster_y_pos))
                if self.appearance_init > 80 and self.appearance_init < 150:
                    self.rect_sea_monster.x = self.sea_monster_x_pos + 100
                    self.rect_sea_monster.y = self.sea_monster_y_pos + 40
                    self.rect_sea_monster.width = 80
                    self.rect_sea_monster.height = 130
                    #pygame.draw.rect(screen, 'red', (self.rect_sea_monster.x, self.rect_sea_monster.y, self.rect_sea_monster.width, self.rect_sea_monster.height), 2)
                else:
                    self.rect_sea_monster.x = 0
                    self.rect_sea_monster.y = 0
                    self.rect_sea_monster.width = 0
                    self.rect_sea_monster.height = 0

                if self.appearance_init == self.appearance:
                    self.sea_monster_x_pos = randrange(10, W - 200)
                    self.sea_monster_y_pos = randrange(H // 3, int(H * 0.50))
                    self.rect_sea_monster = self.obstacle_img.get_rect()
                    self.rect_sea_monster.x = W
                    self.rect_sea_monster.y = self.sea_monster_y_pos
                    self.play_sound_sea_monster = True
                    self.appearance_init = 0
                    self.wait = random.randint(50, 300)
                    self.init_wait = 0
                    self.index = 0
                else:
                    self.appearance_init += 1

                if self.ANIMATION_COOLDOWN > 50:
                    self.index += 1
                    self.ANIMATION_COOLDOWN = 0
                if self.index >= len(self.obstacle_images):
                    self.index = 0
                self.obstacle_img = self.obstacle_images[self.index]
                self.ANIMATION_COOLDOWN += 5
            else:
                self.init_wait += 1

        elif self.image == 'lightning':
            self.obstacle_img = self.obstacle_images[self.index]

            if self.init_wait == self.wait_lightning:
                screen.blit(self.obstacle_img, (self.lightning_x_pos, self.lightning_y_pos))
                #print('Lightning rect x: ', self.rect_lightning.x, '   y: ', self.rect_lightning.y, '   width: ', self.rect_lightning.width, '   height: ', self.rect_lightning.height)
                if self.lightning_appearance_init > 80 and self.lightning_appearance_init < 100:
                    self.rect_lightning.x = self.lightning_x_pos + 40
                    self.rect_lightning.y = self.lightning_y_pos + 280
                    self.rect_lightning.width = 140
                    self.rect_lightning.height = 480
                    #pygame.draw.rect(screen, 'red', (self.rect_lightning.x, self.rect_lightning.y, self.rect_lightning.width, self.rect_lightning.height), 2)
                else:
                    self.rect_lightning.x = 0
                    self.rect_lightning.y = 0
                    self.rect_lightning.width = 0
                    self.rect_lightning.height = 0

                if self.lightning_appearance_init == self.lightning_appearance:
                    self.lightning_x_pos = randrange(10, W - 200)
                    self.lightning_y_pos = 0
                    self.rect_lightning = self.obstacle_img.get_rect()
                    self.rect_lightning.x = W
                    self.rect_lightning.y = self.lightning_y_pos
                    self.lightning_appearance_init = 0
                    self.wait = random.randint(50, 300)
                    self.init_wait = 0
                    self.index = 0
                    self.play_sound_lightning = True
                else:
                    self.lightning_appearance_init += 1

                if self.ANIMATION_COOLDOWN > 50:
                    self.index += 1
                    self.ANIMATION_COOLDOWN = 0
                if self.index >= len(self.obstacle_images):
                    self.index = 0
                self.obstacle_img = self.obstacle_images[self.index]
                self.ANIMATION_COOLDOWN += 10
            else:
                self.init_wait += 1

        elif self.image == 'raven_enemy1':
            self.obstacle_img = self.obstacle_images[self.index]
            screen.blit(self.obstacle_img, (self.raven_enemy1_x_pos, self.raven_enemy1_y_pos))
            self.raven_enemy1_x_pos -= self.obstacle_speed
            self.rect_raven_enemy1.x -= self.obstacle_speed
            self.raven_enemy1_y_pos += self.obstacle_speed
            self.rect_raven_enemy1.y += self.obstacle_speed
            self.rect_raven_enemy1.width = 40
            self.rect_raven_enemy1.height = 40

            if self.raven_enemy1_x_pos <= -3000:
                self.raven_enemy1_x_pos = randrange(W // 2, W)
                self.raven_enemy1_y_pos = 0
                self.rect_raven_enemy1.x = self.raven_enemy1_x_pos + 25
                self.rect_raven_enemy1.y = self.raven_enemy1_y_pos + 15
                self.play_sound_raven_enemy1 = True

            if self.ANIMATION_COOLDOWN > 50:
                self.index += 1
                self.ANIMATION_COOLDOWN = 0
            if self.index >= len(self.obstacle_images):
                self.index = 0
            self.obstacle_img = self.obstacle_images[self.index]
            self.ANIMATION_COOLDOWN += 50

        '''
        elif self.image == 'raven_enemy2':
            self.obstacle_img = self.obstacle_images[self.index]
            screen.blit(self.obstacle_img, (self.raven_enemy2_x_pos, self.raven_enemy2_y_pos))
            self.raven_enemy2_x_pos += self.obstacle_speed
            self.rect_raven_enemy2.x += self.obstacle_speed
            self.raven_enemy2_y_pos += self.obstacle_speed
            self.rect_raven_enemy2.y += self.obstacle_speed
            self.rect_raven_enemy2.width = 40
            self.rect_raven_enemy2.height = 40

            if self.raven_enemy2_x_pos >= 3000:
                self.raven_enemy2_x_pos = randrange(0, W // 2)
                self.raven_enemy2_y_pos = 0
                self.rect_raven_enemy2.x = self.raven_enemy2_x_pos + 15
                self.rect_raven_enemy2.y = self.raven_enemy2_y_pos + 25
                self.play_sound_raven_enemy2 = True

            if self.ANIMATION_COOLDOWN > 50:
                self.index += 1
                self.ANIMATION_COOLDOWN = 0
            if self.index >= len(self.obstacle_images):
                self.index = 0
            self.obstacle_img = self.obstacle_images[self.index]
            self.ANIMATION_COOLDOWN += 50

        elif self.image == 'raven':
            if self.first_spawn_wait == self.first_spawn:
                self.obstacle_img = self.obstacle_images[self.index]
                screen.blit(self.obstacle_img, (self.background_x_pos, self.background_y_pos))
                self.background_x_pos -= self.obstacle_speed
                #if self.play_sound_raven == True:
                #    pygame.mixer.Channel(3).play(pygame.mixer.Sound(resource_path('sound/raven.mp3')))
                #    self.play_sound_raven = False

                if self.background_x_pos <= -50:
                    if self.init_wait == self.wait:
                        self.background_x_pos = W
                        self.background_y_pos = randrange(10, int(180))
                        self.play_sound_raven = True
                        self.init_wait = 0
                        self.wait = random.randint(300, 900)
                    else:
                        self.init_wait += 1

                if self.ANIMATION_COOLDOWN > 50:
                    self.index += 1
                    self.ANIMATION_COOLDOWN = 0
                if self.index >= len(self.obstacle_images):
                    self.index = 0
                self.obstacle_img = self.obstacle_images[self.index]
                self.ANIMATION_COOLDOWN += 50
            else:
                self.first_spawn_wait += 1

        elif self.image == 'raven2':
            if self.first_spawn_wait == self.raven2_first_spawn:
                self.obstacle_img = self.obstacle_images[self.index]
                screen.blit(self.obstacle_img, (self.raven2_background_x_pos, self.raven2_background_y_pos))
                self.raven2_background_x_pos += self.obstacle_speed
                #if self.play_sound_raven2 == True:
                #    pygame.mixer.Channel(5).play(pygame.mixer.Sound(resource_path('sound/raven.mp3')))
                #    self.play_sound_raven2 = False

                if self.raven2_background_x_pos >= W + 100:
                    if self.init_wait == self.wait:
                        self.raven2_background_x_pos = -40
                        self.raven2_background_y_pos = randrange(10, int(180))
                        self.play_sound_raven2 = True
                        self.init_wait = 0
                        self.wait = random.randint(1200, 1300)
                    else:
                        self.init_wait += 1

                if self.ANIMATION_COOLDOWN > 50:
                    self.index += 1
                    self.ANIMATION_COOLDOWN = 0
                if self.index >= len(self.obstacle_images):
                    self.index = 0
                self.obstacle_img = self.obstacle_images[self.index]
                self.ANIMATION_COOLDOWN += 20
            else:
                self.first_spawn_wait += 1


        elif self.image == 'viking_ship':
            self.obstacle_img = self.obstacle_images[self.index]
            screen.blit(self.obstacle_img, (self.viking_ship_x_pos, self.viking_ship_y_pos))
            self.viking_ship_x_pos -= self.obstacle_speed
            self.rect_viking_ship.x -= self.obstacle_speed
            self.rect_viking_ship.width = 230
            self.rect_viking_ship.height = 90

            if self.viking_ship_x_pos <= -3000:
                self.viking_ship_x_pos = W
                self.viking_ship_y_pos = randrange(H // 3, int(H * 0.95))
                self.rect_viking_ship.x = W + 20
                self.rect_viking_ship.y = self.viking_ship_y_pos + 30
                self.play_sound_viking_ship = True

            if self.ANIMATION_COOLDOWN > 50:
                self.index += 1
                self.ANIMATION_COOLDOWN = 0
            if self.index >= len(self.obstacle_images):
                self.index = 0
            self.obstacle_img = self.obstacle_images[self.index]
            self.ANIMATION_COOLDOWN += 10

        elif self.image == 'sleipnir_enemy':
            self.obstacle_img = self.obstacle_images[self.index]
            screen.blit(self.obstacle_img, (self.sleipnir_enemy_x_pos, self.sleipnir_enemy_y_pos))
            self.sleipnir_enemy_x_pos -= self.obstacle_speed
            self.rect_sleipnir_enemy.x -= self.obstacle_speed
            self.rect_sleipnir_enemy.width = 170
            self.rect_sleipnir_enemy.height = 90

            if self.sleipnir_enemy_x_pos <= -3000:
                self.sleipnir_enemy_x_pos = W
                self.sleipnir_enemy_y_pos = randrange(H // 3, int(H * 0.95))
                self.rect_sleipnir_enemy.x = W
                self.rect_sleipnir_enemy.y = self.sleipnir_enemy_y_pos
                self.play_sound_sleipnir_enemy = True

            if self.ANIMATION_COOLDOWN > 50:
                self.index += 1
                self.ANIMATION_COOLDOWN = 0
            if self.index >= len(self.obstacle_images):
                self.index = 0
            self.obstacle_img = self.obstacle_images[self.index]
            self.ANIMATION_COOLDOWN += 10

        elif self.image == 'flying_object':
            self.obstacle_img = self.obstacle_images[self.index]

            if self.init_wait == self.wait_flying_object:
                screen.blit(self.obstacle_img, (self.flying_object_x_pos, self.flying_object_y_pos))
                #print('x: ', self.rect_flying_object.x, '   y: ', self.rect_flying_object.y, '   width: ', self.rect_flying_object.width, '   height: ', self.rect_flying_object.height)
                if self.flying_object_appearance_init > 90 and self.flying_object_appearance_init < 115:
                    self.rect_flying_object.x = self.flying_object_x_pos
                    self.rect_flying_object.y = self.flying_object_y_pos + 670
                    self.rect_flying_object.width = 130
                    self.rect_flying_object.height = 90
                    #pygame.draw.rect(screen, 'red', (self.rect_flying_object.x, self.rect_flying_object.y, self.rect_flying_object.width, self.rect_flying_object.height), 2)
                else:
                    self.rect_flying_object.x = 0
                    self.rect_flying_object.y = 0
                    self.rect_flying_object.width = 0
                    self.rect_flying_object.height = 0

                if self.flying_object_appearance_init == self.flying_object_appearance:
                    self.flying_object_x_pos = randrange(10, W - 200)
                    self.flying_object_y_pos = -random.randrange(400)
                    self.rect_flying_object = self.obstacle_img.get_rect()
                    self.rect_flying_object.x = W
                    self.rect_flying_object.y = self.flying_object_y_pos
                    self.flying_object_appearance_init = 0
                    self.wait = random.randint(50, 300)
                    self.init_wait = 0
                    self.index = 0
                    self.play_sound_flying_object = True

                else:
                    self.flying_object_appearance_init += 1

                if self.ANIMATION_COOLDOWN > 50:
                    self.index += 1
                    self.ANIMATION_COOLDOWN = 0
                if self.index >= len(self.obstacle_images):
                    self.index = 0
                self.obstacle_img = self.obstacle_images[self.index]
                self.ANIMATION_COOLDOWN += 15
            else:
                self.init_wait += 1

        elif self.image == 'lightning_background':
            self.obstacle_img = self.obstacle_images[self.index]

            if self.init_wait == self.wait_lightning_background:
                screen.blit(self.obstacle_img, (self.lightning_background_x_pos, self.lightning_background_y_pos))
                #print('Lightning rect x: ', self.rect_lightning.x, '   y: ', self.rect_lightning.y, '   width: ', self.rect_lightning.width, '   height: ', self.rect_lightning.height)

                if self.lightning_background_appearance_init == self.lightning_background_appearance:
                    self.lightning_background_x_pos = randrange(10, W - 200)
                    self.lightning_background_y_pos = 0
                    self.lightning_background_appearance_init = 0
                    self.wait = random.randint(50, 150)
                    self.init_wait = 0
                    self.index = 0
                    self.play_sound_lightning = True
                else:
                    self.lightning_background_appearance_init += 1

                if self.ANIMATION_COOLDOWN > 50:
                    self.index += 1
                    self.ANIMATION_COOLDOWN = 0
                if self.index >= len(self.obstacle_images):
                    self.index = 0
                self.obstacle_img = self.obstacle_images[self.index]
                self.ANIMATION_COOLDOWN += 10
            else:
                self.init_wait += 1
            '''

def collide_dect():
    if player.player_is_invincible == False:
        #if player.rect_player.colliderect(obstacle1.rect_fireball) or player.rect_player.colliderect(obstacle2.rect_rock) or player.rect_player.colliderect(obstacle3.rect_axe) or player.rect_player.colliderect(obstacle4.rect_sea_monster) or player.rect_player.colliderect(obstacle9.rect_viking_ship) or player.rect_player.colliderect(obstacle13.rect_flying_object) or player.rect_player.colliderect(obstacle14.rect_lightning) or player.rect_player.colliderect(obstacle16.rect_sleipnir_enemy) or player.rect_player.colliderect(obstacle17.rect_raven_enemy1) or player.rect_player.colliderect(obstacle18.rect_raven_enemy2):
        if player.rect_player.colliderect(obstacle2.rect_rock) or player.rect_player.colliderect(obstacle3.rect_axe) or player.rect_player.colliderect(obstacle4.rect_sea_monster) or player.rect_player.colliderect(obstacle14.rect_lightning) or player.rect_player.colliderect(obstacle17.rect_raven_enemy1):
            player.player_lifes -= 1
            if player.player_lifes == 0:
                player.player_is_alive = False
            else:
                player.player_is_invincible = True

'''
def paused():
    text_surface_score = pixel_font.render('Paused', False, (255, 255, 255))
    screen.blit(text_surface_score, (W // 2 - 100, H // 2))

    pause = True
    while pause:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_p:
                    mixer.music.unpause()
                    pygame.mixer.unpause()
                    pause = False
        pygame.display.update()
'''

'''
def game_quit():
    quit_to_main_menu = pixel_font.render('Quit to main menu?', False, (255, 255, 255))
    screen.blit(quit_to_main_menu, (W // 2 - 275, H // 2.5))
    yes_no = pixel_font.render('(Y)es     (N)o', False, (255, 255, 255))
    screen.blit(yes_no, (W // 2 - 175, H // 2.5 + 100))


    global stop_game
    stop_game = False
    pause = True
    while pause:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE or event.key == pygame.K_n:
                    mixer.music.unpause()
                    pygame.mixer.unpause()
                    pause = False
                elif event.key == pygame.K_y:
                    stop_game = True
                    return stop_game
                    pause = False

        pygame.display.update()
'''

class Animated_water(pygame.sprite.Sprite):
    def __init__(self, intro):
        self.intro = intro
        self.index = 0
        self.animated_water_img = pygame.image.load(resource_path('images/animated_water/animated_water_1.png'))
        #animated_water_img = pygame.transform.scale(animated_water_img, (300, 110))
        self.ANIMATION_COOLDOWN = 0
        self.ANIMATION_LENGTH = 1000
        self.i = 0

        ### Load images
        self.animated_water_img1 = pygame.image.load(resource_path('images/animated_water/animated_water_1.png')).convert_alpha()
        self.animated_water_img2 = pygame.image.load(resource_path('images/animated_water/animated_water_2.png')).convert_alpha()
        self.animated_water_img3 = pygame.image.load(resource_path('images/animated_water/animated_water_3.png')).convert_alpha()
        self.animated_water_img4 = pygame.image.load(resource_path('images/animated_water/animated_water_4.png')).convert_alpha()
        self.animated_water_img5 = pygame.image.load(resource_path('images/animated_water/animated_water_5.png')).convert_alpha()
        self.animated_water_img6 = pygame.image.load(resource_path('images/animated_water/animated_water_6.png')).convert_alpha()
        self.animated_water_img7 = pygame.image.load(resource_path('images/animated_water/animated_water_7.png')).convert_alpha()
        self.animated_water_img8 = pygame.image.load(resource_path('images/animated_water/animated_water_8.png')).convert_alpha()
        self.animated_water_img9 = pygame.image.load(resource_path('images/animated_water/animated_water_9.png')).convert_alpha()
        self.animated_water_img10 = pygame.image.load(resource_path('images/animated_water/animated_water_10.png')).convert_alpha()
        self.animated_water_img11 = pygame.image.load(resource_path('images/animated_water/animated_water_11.png')).convert_alpha()
        self.animated_water_img12 = pygame.image.load(resource_path('images/animated_water/animated_water_12.png')).convert_alpha()
        self.animated_water_img13 = pygame.image.load(resource_path('images/animated_water/animated_water_13.png')).convert_alpha()
        self.animated_water_img14 = pygame.image.load(resource_path('images/animated_water/animated_water_14.png')).convert_alpha()
        self.animated_water_img15 = pygame.image.load(resource_path('images/animated_water/animated_water_15.png')).convert_alpha()
        self.animated_water_img16 = pygame.image.load(resource_path('images/animated_water/animated_water_16.png')).convert_alpha()
        self.animated_water_img17 = pygame.image.load(resource_path('images/animated_water/animated_water_17.png')).convert_alpha()
        ### Append images to list
        self.animated_water = []
        self.animated_water.append(self.animated_water_img1)
        self.animated_water.append(self.animated_water_img2)
        self.animated_water.append(self.animated_water_img3)
        self.animated_water.append(self.animated_water_img4)
        self.animated_water.append(self.animated_water_img5)
        self.animated_water.append(self.animated_water_img6)
        self.animated_water.append(self.animated_water_img7)
        self.animated_water.append(self.animated_water_img8)
        self.animated_water.append(self.animated_water_img9)
        self.animated_water.append(self.animated_water_img10)
        self.animated_water.append(self.animated_water_img11)
        self.animated_water.append(self.animated_water_img12)
        self.animated_water.append(self.animated_water_img13)
        self.animated_water.append(self.animated_water_img14)
        self.animated_water.append(self.animated_water_img15)
        self.animated_water.append(self.animated_water_img16)
        self.animated_water.append(self.animated_water_img17)

    def show_animated_water(self):
        ### Place background image
        screen.blit(self.animated_water_img, (self.i, 80))
        screen.blit(self.animated_water_img, (W + self.i, 80))

        if self.intro == True:
            if (self.i <= -W):
                screen.blit(self.animated_water_img, (W + self.i, 80))
                self.i = 0
            self.i -= 5

        #screen.blit(self.animated_water_img, (0, 240))
        if self.ANIMATION_COOLDOWN > 100:
            self.index += 1
            self.ANIMATION_COOLDOWN = 0
        if self.index >= len(self.animated_water):
            self.index = 0
        self.animated_water_img = self.animated_water[self.index]
        self.ANIMATION_COOLDOWN += 10

#def game():
async def main():
    sound_state = False
    music_state = True
    difficulty = 1
    #character = 2
    current_player = 'Pyhalla'

    #current_player = user_name.get_value()
    #pygame.mixer.Channel(1).set_volume(0.3)
    #pygame.mixer.Channel(2).set_volume(1)
    #pygame.mixer.Channel(3).set_volume(1)
    #pygame.mixer.Channel(4).set_volume(1)
    #pygame.mixer.Channel(5).set_volume(1) ### raven2
    #pygame.mixer.Channel(6).set_volume(1) ### Flying object
    #pygame.mixer.Channel(7).set_volume(1) ### Viking ship
    #pygame.mixer.Channel(8).set_volume(1) ### Lightning
    #pygame.mixer.Channel(9).set_volume(0.3) ### Collision and game over sounds
    #pygame.mixer.Channel(10).set_volume(1) ### Game start sound
    #pygame.mixer.Channel(11).set_volume(1) ### Player hit sound
    #pygame.mixer.Channel(12).set_volume(0.7) ### Sleipnir
    #pygame.mixer.Channel(13).set_volume(0.7) ### Sleipnir enemy
    #pygame.mixer.Channel(14).set_volume(1) ### Raven enemy 1
    #pygame.mixer.Channel(15).set_volume(1) ### Raven enemy 2

    global time_passed
    time_passed = 0
    score = 0
    score_tick = 500
    score_init = 0
    highscore = 0

    ### Create Player character
    global player
    player = Player()

    ### Create Water
    global animated_water
    animated_water = Animated_water(True)
    animated_water_intro = Animated_water(False)

    ### Initiate Movement
    up_slittering = False
    down_slittering = False
    left_slittering = False
    right_slittering = False
    currently_moving_up = False
    currently_moving_down = False
    currently_moving_left = False
    currently_moving_right = False

    ### Create Obstacle
    global obstacle1
    global obstacle2
    global obstacle3
    global obstacle4
    global obstacle5
    global obstacle9
    global obstacle13
    global obstacle14
    global obstacle16
    global obstacle17
    global obstacle18
    global obstacle19
    #obstacle1 = Obstacle(10, 'fireball')
    obstacle2 = Obstacle(2, 'rock')
    obstacle3 = Obstacle(6, 'axe')
    obstacle4 = Obstacle(0, 'sea_monster')
    #obstacle5 = Obstacle(7, 'raven')
    #obstacle11 = Obstacle(2, 'raven2')
    #obstacle9 = Obstacle(12, 'viking_ship')
    #obstacle13 = Obstacle(0, 'flying_object')
    obstacle14 = Obstacle(0, 'lightning')
    #obstacle16 = Obstacle(12, 'sleipnir_enemy')
    obstacle17 = Obstacle(6, 'raven_enemy1')
    #obstacle18 = Obstacle(6, 'raven_enemy2')

    global running
    running = True
    sky_speed = 0
    i = 0
    sky_draw_init = False
    bg_draw_init = False

    ### Special ability
    special_ability_init = 0
    special_ability_duration = 300
    special_ability_timer = 1500
    global special_ability
    special_ability = False

    global intro_running
    global intro_duration
    global init_intro_duration
    global char_x
    intro_running = True
    intro_duration = 150
    init_intro_duration = 0
    char_x = -300

    pygame.mixer.Channel(0).set_volume(0)  ### Turn of main menu music

    if sound_state == True:
        ### Play game start sound
        if character == 2:
            pygame.mixer.Channel(10).play(pygame.mixer.Sound(resource_path('sound/game_start.mp3')))
        else:
            pygame.mixer.Channel(10).play(pygame.mixer.Sound(resource_path('sound/player_char_horn.mp3')))
    '''
    while intro_running:
        ### Set FPS to 60
        clock.tick(FPS)

        ### Make Background of window black
        screen.fill((0, 0, 0))
        ### Sky background
        screen.blit(sky2, (i, -135))
        ### Background
        screen.blit(bg2, (i, -135))

        ### Show animated water
        animated_water_intro.show_animated_water()

        if character == 2:
            intro_ship_shadow = pygame.image.load(resource_path('images/ship_shadow.png')).convert_alpha()
            intro_player_img = pygame.image.load(resource_path('images/player_char.png')).convert_alpha()
            ship_sail_img1 = pygame.image.load(resource_path('images/ship_sail/sail1.png')).convert_alpha()
            screen.blit(intro_ship_shadow, (char_x - 40, player.char_y - 270))
            screen.blit(ship_sail_img1, (char_x - 40, player.char_y - 350))
        elif character == 1:
            intro_ship_shadow = pygame.image.load(resource_path('images/ship_shadow.png')).convert_alpha()
            intro_player_char_viking = pygame.image.load(resource_path('images/player_char/player_char_viking_horn.png')).convert_alpha()
            intro_player_img = pygame.image.load(resource_path('images/player_char/player_char1.png')).convert_alpha()
            ship_sail_img1 = pygame.image.load(resource_path('images/ship_sail/sail1.png')).convert_alpha()
            screen.blit(intro_ship_shadow, (char_x - 30, player.char_y - 25))
            screen.blit(ship_sail_img1, (char_x - 30, 350))
            screen.blit(intro_player_char_viking, (char_x + 120, 447.5))
        screen.blit(intro_player_img, (char_x, 200))

        for event in pygame.event.get():
            ### Stop game when hitting the X
            if event.type == pygame.QUIT:
                pygame.mixer.music.stop()
                running = False

        if char_x != 0:
            char_x += 3

        if init_intro_duration != intro_duration:
            init_intro_duration += 1
        else:
            intro_running = False
    '''

    if music_state == True:
        mixer.music.load(resource_path('sound/game_music.mp3'))
        mixer.music.set_volume(0.15)
        mixer.music.play(-1)

    while running:
        clock.tick(FPS)

        ### Key interaction
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP]:
            currently_moving_up = True
            player.moving_up()
        if keys[pygame.K_DOWN]:
            currently_moving_down = True
            player.moving_down()
        if keys[pygame.K_RIGHT]:
            currently_moving_right = True
            player.moving_right()
        if keys[pygame.K_LEFT]:
            currently_moving_left = True
            player.moving_left()

        ### Make Background of window black
        screen.fill((0, 0, 0))
        if sky_draw_init == True:
            ### Place background image
            screen.blit(sky1, (sky_speed, 0))
            screen.blit(sky2, (W + sky_speed, 0))
        else:
            screen.blit(sky2, (sky_speed, 0))
            screen.blit(sky1, (W + sky_speed, 0))

        if bg_draw_init == True:
            ### Place background image
            screen.blit(bg1, (i, 0))
            screen.blit(bg2, (W + i, 0))
        else:
            screen.blit(bg2, (i, 0))
            screen.blit(bg1, (W + i, 0))

        ### Show animated water
        animated_water.show_animated_water()

        ### Spawn background objects
        #obstacle5.add_obstacle_at_location()
        #obstacle11.add_obstacle_at_location()

        ### Add obstacles and player
        ### Spawn obstacles depending on passed time
        if time_passed > 1000:
            obstacle2.add_obstacle_at_location() ### Rock
        #if time_passed > 1000:
        #    obstacle9.add_obstacle_at_location() ### Viking-ship
        if time_passed > 1000:
            obstacle4.add_obstacle_at_location() ### Sea monster
        if time_passed > 1000:
            obstacle3.add_obstacle_at_location() ### Axe
        #if time_passed > 1000:
        #    obstacle1.add_obstacle_at_location() ### Fireball
        if time_passed > 1000:
            obstacle17.add_obstacle_at_location()  ### Raven enemy 1
        #if time_passed > 95000:
        #    obstacle16.add_obstacle_at_location()  ### Sleipnir enemy
        #if time_passed > 110000:
        #    obstacle18.add_obstacle_at_location()  ### Raven enemy 2

        ### Draw player character
        player.spawn_character()

        ### Add obstacles and player
        ### Spawn obstacles depending on passed time
        #if time_passed > 1000:
        #    obstacle13.add_obstacle_at_location()  ### Flying object
        if time_passed > 2000:
            obstacle14.add_obstacle_at_location()  ### Lightning

        ### Draw Score
        print_score = pixel_font.render('Glory: ' + str(score), False, (255, 255, 255))
        screen.blit(print_score, (0, 0))

        print_highscore = pixel_font.render('Highscore: ' + str(highscore), False, (255, 255, 255))
        screen.blit(print_highscore, (0, 50))

        ### Check for collisions
        collide_dect()

        ### Manage player lifes
        print_lifes = pixel_font.render('Lifes: ', False, (255, 255, 255))
        screen.blit(print_lifes, (W - 260, 0))
        if player.player_lifes >= 3:
            if character == 1:
                player_char_lifes = pygame.image.load(resource_path('images/player_char/player_lifes3.png')).convert_alpha()
                screen.blit(player_char_lifes, (W - 150, 0))
            elif character == 2:
                player_char_lifes = pygame.image.load(resource_path('images/character3_player_lifes3.png')).convert_alpha()
                screen.blit(player_char_lifes, (W - 150, 0))
        elif player.player_lifes == 2:
            if character == 1:
                player_char_lifes = pygame.image.load(resource_path('images/player_char/player_lifes2.png')).convert_alpha()
                screen.blit(player_char_lifes, (W - 150, 0))
            elif character == 2:
                player_char_lifes = pygame.image.load(resource_path('images/character3_player_lifes2.png')).convert_alpha()
                screen.blit(player_char_lifes, (W - 150, 0))
        elif player.player_lifes == 1:
            if character == 1:
                player_char_lifes = pygame.image.load(resource_path('images/player_char/player_lifes1.png')).convert_alpha()
                screen.blit(player_char_lifes, (W - 150, 0))
            elif character == 2:
                player_char_lifes = pygame.image.load(resource_path('images/character3_player_lifes1.png')).convert_alpha()
                screen.blit(player_char_lifes, (W - 150, 0))
        elif player.player_lifes == 0:
            pass
        player.player_img_default.set_alpha(255)
        if character == 1:
            player.player_ship_shadow.set_alpha(255)
            player.player_char_viking_images_img1.set_alpha(255)
            player.ship_sail_img1.set_alpha(255)
        elif character == 2:
            player.ship_sail_img1.set_alpha(255)
        if player.player_is_invincible == True:
            if player.invincible_init < player.invincible_timer:
                player.invincible_init += 1
                player.player_img_default.set_alpha(128)
                if character == 1:
                    player.player_ship_shadow.set_alpha(128)
                    player.player_char_viking_images_img1.set_alpha(128)
                    player.ship_sail_img1.set_alpha(128)
                    player.player_char_viking_horn.set_alpha(128)
                    player.player_char_viking_lookup.set_alpha(128)
                    player.player_char_viking_lookdown.set_alpha(128)
                elif character == 2:
                    player.player_ship_shadow.set_alpha(128)
                    player.ship_sail_img1.set_alpha(128)
            else:
                player.player_is_invincible = False
                player.invincible_init = 0
                if character == 1:
                    player.player_ship_shadow.set_alpha(255)
                    player.player_char_viking_horn.set_alpha(255)
                    player.player_char_viking_lookup.set_alpha(255)
                    player.player_char_viking_lookdown.set_alpha(255)
                elif character == 2:
                    player.player_ship_shadow.set_alpha(255)

        ### Show special ability
        if special_ability_init >= special_ability_timer:
            special_ability_ready = pygame.image.load(resource_path('images/special_ability_ready.png')).convert_alpha()
            screen.blit(special_ability_ready, (W // 2.5, 0))
        else:
            special_ability_charging = pygame.image.load(resource_path('images/special_ability_charging.png')).convert_alpha()
            screen.blit(special_ability_charging, (W // 2.5, 0))

        ### Update sky background
        if (sky_speed == -W):
            if sky_draw_init == True:
                sky_draw_init = False
            elif sky_draw_init == False:
                sky_draw_init = True
            sky_speed = 0
        sky_speed -= 0.5
        ### Update window and display background
        if (i==-W):
            if bg_draw_init == True:
                bg_draw_init = False
            elif bg_draw_init == False:
                bg_draw_init = True
            i = 0
        i -= 1

        for event in pygame.event.get():
            ### Stop game when hitting the X
            if event.type == pygame.QUIT:
                pygame.mixer.music.stop()
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    if special_ability_init >= special_ability_timer:
                        special_ability_init = 0
                        player.player_is_invincible = True
                        #if character == 1:
                        #    player.player_is_invincible = True
                        #elif character == 2:
                        #    pass
            #        special_ability = True
            #    if event.key == pygame.K_p:
            #        mixer.music.pause()
            #        pygame.mixer.pause()
            #        paused()
                #elif event.key == pygame.K_ESCAPE:
                #    mixer.music.pause()
                #    pygame.mixer.pause()
                #    game_quit()
                #    if stop_game == True:
                #        pygame.mixer.music.stop()
                #        running = False
                #        #pygame.mixer.Channel(0).play(pygame.mixer.Sound(resource_path('sound/main_menu.mp3')), (-1))
                #        if mute_music == False:
                #            pygame.mixer.Channel(0).set_volume(0.5)
                #if event.key == pygame.K_SPACE:
                #    if special_ability_init >= special_ability_timer:
                #        special_ability_init = 0
                #        if character == 1:
                #            player.player_is_invincible = True
                #        elif character == 2:
                #            pass
                #        special_ability = True

            if event.type == pygame.KEYUP:
                key = pygame.key.name(event.key)
                if key == 'up':
                    currently_moving_up = False
                    up_slittering = True
                if key == 'down':
                    currently_moving_down = False
                    down_slittering = True
                if key == 'left':
                    currently_moving_left = False
                    left_slittering = True
                if key == 'right':
                    currently_moving_right = False
                    right_slittering = True

        if up_slittering:
            if player.char_y > 350 and currently_moving_up == False:
                if 80 < player.smooth_movement_up <= 100:
                    player.char_y -= 5
                    player.rect_player.y -= 5
                    player.smooth_movement_up -= 2
                if 60 < player.smooth_movement_up <= 80:
                    player.char_y -= 4
                    player.rect_player.y -= 4
                    player.smooth_movement_up -= 2
                if 40 < player.smooth_movement_up <= 60:
                    player.char_y -= 3
                    player.rect_player.y -= 3
                    player.smooth_movement_up -= 2
                if 20 < player.smooth_movement_up <= 40:
                    player.char_y -= 2
                    player.rect_player.y -= 2
                    player.smooth_movement_up -= 2
                if 0 < player.smooth_movement_up <= 20:
                    player.char_y -= 1
                    player.rect_player.y -= 1
                    player.smooth_movement_up -= 2
                if player.smooth_movement_up == 0:
                    up_slittering = False
            elif player.char_y == 350:
                player.smooth_movement_up = 0
                up_slittering = False

        if down_slittering:
            if player.char_y < 550 and currently_moving_down == False:
                if 80 < player.smooth_movement_down <= 100:
                    player.char_y += 5
                    player.rect_player.y += 5
                    player.smooth_movement_down -= 2
                if 60 < player.smooth_movement_down <= 80:
                    player.char_y += 4
                    player.rect_player.y += 4
                    player.smooth_movement_down -= 2
                if 40 < player.smooth_movement_down <= 60:
                    player.char_y += 3
                    player.rect_player.y += 3
                    player.smooth_movement_down -= 2
                if 20 < player.smooth_movement_down <= 40:
                    player.char_y += 2
                    player.rect_player.y += 2
                    player.smooth_movement_down -= 2
                if 0 < player.smooth_movement_down <= 20:
                    player.char_y += 1
                    player.rect_player.y += 1
                    player.smooth_movement_down -= 2
                if player.smooth_movement_down == 0:
                    down_slittering = False
            elif player.char_y == 550:
                player.smooth_movement_down = 0
                down_slittering = False

        if left_slittering:
            if player.char_x > 0 and currently_moving_left == False:
                if 80 < player.smooth_movement_left <= 100:
                    player.char_x -= 5
                    player.rect_player.x -= 5
                    player.smooth_movement_left -= 2
                if 60 < player.smooth_movement_left <= 80:
                    player.char_x -= 4
                    player.rect_player.x -= 4
                    player.smooth_movement_left -= 2
                if 40 < player.smooth_movement_left <= 60:
                    player.char_x -= 3
                    player.rect_player.x -= 3
                    player.smooth_movement_left -= 2
                if 20 < player.smooth_movement_left <= 40:
                    player.char_x -= 2
                    player.rect_player.x -= 2
                    player.smooth_movement_left -= 2
                if 0 < player.smooth_movement_left <= 20:
                    player.char_x -= 1
                    player.rect_player.x -= 1
                    player.smooth_movement_left -= 2
                if player.smooth_movement_left == 0:
                    left_slittering = False
            elif player.char_x == 0:
                player.smooth_movement_left = 0
                left_slittering = False

        if right_slittering:
            if player.char_x <= W - 220 and currently_moving_right == False:
                if 80 < player.smooth_movement_right <= 100:
                    player.char_x += 5
                    player.rect_player.x += 5
                    player.smooth_movement_right -= 2
                if 60 < player.smooth_movement_right <= 80:
                    player.char_x += 4
                    player.rect_player.x += 4
                    player.smooth_movement_right -= 2
                if 40 < player.smooth_movement_right <= 60:
                    player.char_x += 3
                    player.rect_player.x += 3
                    player.smooth_movement_right -= 2
                if 20 < player.smooth_movement_right <= 40:
                    player.char_x += 2
                    player.rect_player.x += 2
                    player.smooth_movement_right -= 2
                if 0 < player.smooth_movement_right <= 20:
                    player.char_x += 1
                    player.rect_player.x += 1
                    player.smooth_movement_right -= 2
                if player.smooth_movement_right == 0:
                    right_slittering = False
                elif player.char_y == W - 220:
                    player.smooth_movement_right = 0
                    right_slittering = False

        time_passed = time_passed + 10
        special_ability_init += 1

        if character == 2 and special_ability == True:
            if round(score_init) >= score_tick:
                score += 1
                score_init = 0
            else:
                score_init += (FPS * 1.5)
        else:
            if round(score_init) >= score_tick:
                score += 1
                score_init = 0
            else:
                score_init += 70

        pygame.display.update()

        if player.player_is_alive == False:
            player.player_is_alive = True
            player.player_lifes = 4
            if score > highscore:
                highscore = score
            score = 0

            #running = False
        '''
            #pygame.mixer.music.stop()
            #pygame.mixer.stop()
            #boom_x = player.rect_player.x
            #boom_y = player.rect_player.y
            #boom = pygame.image.load(resource_path('images/boom.png')).convert_alpha()
            #screen.blit(boom, (boom_x + 50, boom_y - 50))
            
            pygame.display.update()

            proceed = True

            while proceed:
                for event in pygame.event.get():
                    screen.blit(game_over, (0, 0))

                    ### Draw Score
                    #norse_font = pygame.freetype.Font("NorseBold.otf", 100)
                    #print_score = pixel_font.render(str(score), False, (255, 255, 255))
                    screen.blit(print_score, (W // 2 - 75, 125))

                    pygame.display.update()
                    if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                        proceed = False

            running = False
        '''
        await asyncio.sleep(0)

asyncio.run(main())
